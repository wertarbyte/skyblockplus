package de.craften.plugins.skyblockplus;

import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.skyblockplus.addons.DynmapAddon;
import de.craften.plugins.skyblockplus.addons.JsonapiAddon;
import de.craften.plugins.skyblockplus.commands.AdminCommands;
import de.craften.plugins.skyblockplus.commands.IslandCommands;
import de.craften.plugins.skyblockplus.commands.TeleportCommands;
import de.craften.plugins.skyblockplus.commands.ToplistCommands;
import de.craften.plugins.skyblockplus.commands.util.SkyblockSubCommandHandler;
import de.craften.plugins.skyblockplus.gui.SkyblockGui;
import de.craften.plugins.skyblockplus.listeners.MobListener;
import de.craften.plugins.skyblockplus.listeners.RespawnListener;
import de.craften.plugins.skyblockplus.persistence.CacheWrapper;
import de.craften.plugins.skyblockplus.persistence.PersistenceException;
import de.craften.plugins.skyblockplus.persistence.SkyblockPersistence;
import de.craften.plugins.skyblockplus.persistence.YamlSkyblockPersistence;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import de.craften.plugins.skyblockplus.skyblock.SkyblockWorld;
import de.craften.plugins.skyblockplus.util.importer.USkyblockImporter;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemoryConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

/**
 * PlotPlus addon of SkyblockPlus. This is the class that connects everything.
 */
public class SkyblockPlus extends JavaPlugin {
    private SkyblockPersistence persistence;
    private SkyblockGui gui;
    private Map<String, SkyblockWorld> worlds;

    @Override
    public void onEnable() {
        persistence = CacheWrapper.wrap(new YamlSkyblockPersistence(
                new File(getDataFolder(), "players"),
                new File(getDataFolder(), "islands")));
        gui = new SkyblockGui(this);

        worlds = loadWorlds();

        PlotPlus.getApi().registerAddon(new SkyblockPlusAddon(this));

        SkyblockSubCommandHandler commandHandler = new SkyblockSubCommandHandler();
        commandHandler.addHandlers(
                new IslandCommands(this),
                new AdminCommands(this),
                new TeleportCommands(this),
                new ToplistCommands(this)
        );
        getCommand("skyblock").setExecutor(commandHandler);
        getServer().getPluginManager().registerEvents(new RespawnListener(this), this);
        getServer().getPluginManager().registerEvents(new MobListener(this), this);

        getServer().getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void onPluginEnabled(PluginEnableEvent event) {
                if (event.getPlugin().getName().equals("dynmap")) {
                    getLogger().info("Dynmap found, enabling dynmap addon");
                    new DynmapAddon(SkyblockPlus.this).enable(getConfig().getConfigurationSection("dynmap"));
                } else if (event.getPlugin().getName().equals("JSONAPI")) {
                    getLogger().info("Jsonapi found, enabling jsonapi addon");
                    new JsonapiAddon(SkyblockPlus.this).enable(getConfig().getConfigurationSection("jsonapi"));
                }
            }
        }, this);

        if (getConfig().getBoolean("import")) {
            USkyblockImporter importer;
            try {
                importer = new USkyblockImporter(this, new File(getDataFolder().getParentFile(), "uSkyBlock"), getWorld("skyworld"));
                importer.importIslands();
                importer.importPlayers();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, SkyblockWorld> loadWorlds() {
        Map<String, SkyblockWorld> worlds = new HashMap<>();
        for (String worldName : getConfig().getConfigurationSection("worlds").getKeys(false)) {
            ConfigurationSection worldConfig = getConfig().getConfigurationSection("worlds." + worldName);

            ConfigurationSection challenges = new MemoryConfiguration();
            for (String filename : worldConfig.getStringList("challengesFiles")) {
                File challengesFile = new File(getDataFolder(), filename);
                if (challengesFile.exists()) {
                    ConfigurationSection list = YamlConfiguration.loadConfiguration(challengesFile);
                    for (String challenge : list.getKeys(false)) {
                        challenges.set(challenge, list.getConfigurationSection(challenge));
                    }
                } else {
                    getLogger().warning("Challenges file '" + filename + "' for " + worldName + " doesn't exist");
                }
            }

            File blockRatingFile = new File(getDataFolder(), worldConfig.getString("blockRatingFile"));
            if (!blockRatingFile.exists()) {
                getLogger().warning("Block rating file for " + worldName + " doesn't exist");
            }
            worlds.put(worldName, new SkyblockWorld(worldName, worldConfig, challenges,
                    YamlConfiguration.loadConfiguration(blockRatingFile)));
        }
        return worlds;
    }

    /**
     * Reloads the configuration and updates worlds and challenges.
     */
    @Override
    public void reloadConfig() {
        super.reloadConfig();
        worlds = loadWorlds();
    }

    @Override
    public void onDisable() {
        gui.closeAll(); //prevents players from moving items out of the inventories after this plugin is unloaded
    }

    /**
     * Gets the persistence.
     *
     * @return the persistence
     */
    public SkyblockPersistence getPersistence() {
        return persistence;
    }

    /**
     * Gets the island at the position of the given player.
     *
     * @param player player
     * @return island at the position of the given player or null if there is no island at that location
     */
    public Island getIslandAt(Player player) {
        SkyblockWorld world = getWorld(player.getWorld());
        if (world != null) {
            return world.getIslandAt(player.getLocation());
        }
        return null;
    }

    /**
     * Gets the {@link de.craften.plugins.skyblockplus.skyblock.SkyblockWorld} for the given bukkit world.
     *
     * @param world world to get the skyblock world for
     * @return skyblock world for the given world or null if it doesn't exist
     */
    public SkyblockWorld getWorld(World world) {
        return getWorld(world.getName());
    }

    /**
     * Gets the {@link de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer} for the given Bukkit player.
     *
     * @param player player to get the skyblock player for
     * @return skyblock player for the given player
     */
    public SkyblockPlayer getPlayer(OfflinePlayer player) {
        try {
            return getPersistence().getPlayer(player.getUniqueId());
        } catch (PersistenceException e) {
            getLogger().log(Level.SEVERE, "Could not load skyblock player!", e);
            return null;
        }
    }

    /**
     * Gets the {@link de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer} with the given UUID.
     *
     * @param uuid UUID of the player to get the skyblock player for
     * @return skyblock player with the given UUID
     */
    public SkyblockPlayer getPlayer(UUID uuid) {
        try {
            return getPersistence().getPlayer(uuid);
        } catch (PersistenceException e) {
            getLogger().log(Level.SEVERE, "Could not load skyblock player!", e);
            return null;
        }
    }

    /**
     * Gets the inventory-based skyblock GUI.
     *
     * @return the skyblock GUI
     */
    public SkyblockGui getGui() {
        return gui;
    }

    /**
     * Gets all skyblock worlds of this server.
     *
     * @return all skyblock worlds of this server
     */
    public static Collection<SkyblockWorld> getWorlds() {
        return SkyblockPlus.getPlugin(SkyblockPlus.class).worlds.values();
    }

    /**
     * Gets the {@link de.craften.plugins.skyblockplus.skyblock.SkyblockWorld} with the given name.
     *
     * @param name name of the world to get the skyblock world for
     * @return skyblock world with the given name or null if it doesn't exist
     */
    public static SkyblockWorld getWorld(String name) {
        return SkyblockPlus.getPlugin(SkyblockPlus.class).worlds.get(name);
    }
}
