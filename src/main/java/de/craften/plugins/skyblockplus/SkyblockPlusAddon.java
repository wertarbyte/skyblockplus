package de.craften.plugins.skyblockplus;

import de.craften.plugins.plotplus.api.ListeningAddon;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.skyblockplus.persistence.PersistenceException;

import java.util.logging.Level;


public class SkyblockPlusAddon extends ListeningAddon {
    private SkyblockPlus plugin;

    public SkyblockPlusAddon(SkyblockPlus plugin) {
        super("SkyblockPlus");
        this.plugin = plugin;
    }

    @Override
    protected void onEnabled() {
        getApi().registerWorldScheme("skyblock", SkyblockWorldScheme.class);
    }

    @Override
    protected void onDisabling() {

    }

    @Override
    public void onPlotClaimed(Plot plot) {
        plugin.getWorld(plot.getWorld().getName()).getIsland(plot.getId()).initialize();
    }

    @Override
    public void onPlotRemoved(Plot plot) {
        try {
            plugin.getPersistence().removeIsland(plugin.getWorld(plot.getWorld().getName()).getIsland(plot.getId()));
        } catch (PersistenceException e) {
            plugin.getLogger().log(Level.SEVERE, "Could not remove island", e);
        }
    }

    @Override
    public void onPlotCleared(Plot plot) {
        plugin.getWorld(plot.getWorld().getName()).getIsland(plot.getId()).initialize();
    }
}
