package de.craften.plugins.skyblockplus;

import de.craften.plugins.plotplus.generator.BlockProvider;
import de.craften.plugins.plotplus.generator.PlotStructure;
import de.craften.plugins.plotplus.generator.blocks.Block;
import de.craften.plugins.plotplus.util.PointXZ;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.jnbt.*;

import java.io.IOException;
import java.util.Map;

/**
 * Skyblock structure used to generate the islands.
 */
public class SkyblockStructure extends PlotStructure implements BlockProvider {
    private int width;
    private int length;
    private byte[] blocks;
    private byte[] data;

    public SkyblockStructure() {
        NBTInputStream nis;
        Map<String, Tag> masterMap;
        try {
            nis = new NBTInputStream(Bukkit.getPluginManager().getPlugin("SkyblockPlus").getResource("island.schematic"));
            CompoundTag master = (CompoundTag) nis.readTag();
            nis.close();
            masterMap = master.getValue();
        } catch (IOException e) {
            throw new IllegalStateException("Could not read island.schematic!", e);
        }

        width = ((ShortTag) masterMap.get("Width")).getValue();
        length = ((ShortTag) masterMap.get("Length")).getValue();
        //height = ((ShortTag) masterMap.get("Height")).getValue();

        blocks = ((ByteArrayTag) masterMap.get("Blocks")).getValue();
        data = ((ByteArrayTag) masterMap.get("Data")).getValue();
    }

    @Override
    public Block getBlockAt(int x, int y, int z) {
        if (y == 120) {
            if (x % 110 == 0 && z % 110 == 0) {
                return new Block(Material.BEDROCK);
            }
        } else {
            PointXZ relPos = getRelativePositionToIsland(x, z);
            if (relPos.getX() >= 0 && relPos.getX() < 7
                    && relPos.getZ() >= 0 && relPos.getZ() < 7
                    && y > 120 && y < 132) {
                int index = (y - 120) * width * length + (relPos.getZ()) * width + (relPos.getX());

                if (blocks.length > index) {
                    short id = (short) (blocks[index] & 0xFF);
                    return new Block(id, data[index]);
                }
            }
        }

        return null;
    }

    private PointXZ getRelativePositionToIsland(int x, int z) {
        return getRelativePosition(x + 3, z + 3);
    }

    @Override
    public int getWidth() {
        return 110;
    }

    @Override
    public int getLength() {
        return 110;
    }
}
