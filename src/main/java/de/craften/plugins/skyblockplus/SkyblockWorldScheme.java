package de.craften.plugins.skyblockplus;

import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.plot.world.PlotWorldScheme;
import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.plotplus.util.WorldUtil;
import de.craften.plugins.plotplus.util.region.RectangularRegion;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.util.Vector;

public class SkyblockWorldScheme extends PlotWorldScheme {

    public SkyblockWorldScheme(ConfigurationSection configuration) {
        super(new SkyblockStructure());
    }

    @Override
    public SkyblockStructure getStructure() {
        return (SkyblockStructure) super.getStructure();
    }

    @Override
    public int getNorthOffset() {
        return 5;
    }

    @Override
    public int getEastOffset() {
        return 5;
    }

    @Override
    public int getSouthOffset() {
        return 5;
    }

    @Override
    public int getWestOffset() {
        return 5;
    }

    @Override
    public boolean isInBuildZone(Location location) {
        PointXZ relPos = getStructure().getRelativePosition((int) location.getX() + 54, (int) location.getZ() + 54);
        return relPos.getX() >= getWestOffset() && relPos.getX() < (getStructure().getWidth() - getEastOffset()) &&
                relPos.getZ() >= getNorthOffset() && relPos.getZ() < (getStructure().getLength() - getSouthOffset());
    }

    @Override
    public PointXZ getPlotIdAt(Location location) {
        return new PointXZ((int) Math.floor((location.getX() + 54) / getStructure().getWidth()),
                (int) Math.floor((location.getZ() + 54) / getStructure().getLength()));
    }

    @Override
    public RectangularRegion getCoreBuildRegion(int idX, int idZ) {
        return new RectangularRegion(
                idX * getStructure().getWidth() + getWestOffset() - 54,
                idZ * getStructure().getLength() + getNorthOffset() - 54,
                idX * getStructure().getWidth() + getStructure().getWidth() - getEastOffset() - 1 - 54,
                idZ * getStructure().getLength() + getStructure().getLength() - getSouthOffset() - 1 - 54
        );
    }

    @Override
    public RectangularRegion getFullPlotRegion(int idX, int idZ) {
        return RectangularRegion.byStartAndSize(
                idX * getStructure().getWidth() - 54,
                idZ * getStructure().getLength() - 54,
                getStructure().getWidth(),
                getStructure().getLength()
        );
    }

    @Override
    public void onLoaded(PlotWorld plotWorld) {
    }

    @Override
    public Location getTeleportLocation(PlotWorld world, int idX, int idZ) {
        World bukkitWorld = world.getBukkitWorld();
        double x = idX * getStructure().getWidth();
        double z = idZ * getStructure().getLength();
        double y = (double) WorldUtil.topmostSafeLocation(bukkitWorld, x, z);
        return new Location(bukkitWorld, x, y, z).add(0.5, 0, 0.5).setDirection(new Vector(0, 0, 1));
    }

    /**
     * Gets the start chest that contains the items a player gets on a new island.
     *
     * @param world world
     * @param idX   x-ID of the island
     * @param idZ   z-coordinate of the island
     * @return the start chest of that island or null if that chest doesn't exist
     */
    public Chest getStartChest(PlotWorld world, int idX, int idZ) {
        World bukkitWorld = world.getBukkitWorld();
        int x = idX * getStructure().getWidth();
        int z = idZ * getStructure().getLength() + 1;
        int y = 125;
        BlockState block = bukkitWorld.getBlockAt(x, y, z).getState();
        if (block instanceof Chest) {
            return (Chest) block;
        }
        return null;
    }

    /**
     * Gets the center position of an island.
     *
     * @param idX x-ID of the island
     * @param idZ z-ID of the island
     * @return center position of the island
     */
    public PointXZ getCenter(int idX, int idZ) {
        return new PointXZ(idX * 110, idZ * 110);
    }
}
