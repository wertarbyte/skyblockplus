package de.craften.plugins.skyblockplus.addons;

import com.google.common.collect.Lists;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.generator.PlotStructure;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.persistence.PersistenceException;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import de.craften.plugins.skyblockplus.skyblock.SkyblockWorld;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;
import org.dynmap.DynmapAPI;
import org.dynmap.markers.Marker;
import org.dynmap.markers.MarkerAPI;
import org.dynmap.markers.MarkerIcon;
import org.dynmap.markers.MarkerSet;

import java.util.*;
import java.util.logging.Level;

/**
 * Add-on that adds a skyblock layer to dynmap.
 */
public class DynmapAddon {
    private final SkyblockPlus plugin;
    private MarkerSet markers;
    private Map<PlotId, Marker> islands = new HashMap<>();
    private List<PlotId> ignoredIslands;
    private MarkerIcon markerIcon;

    /**
     * Creates a new instance of this add-on.
     *
     * @param plugin plugin instance
     */
    public DynmapAddon(SkyblockPlus plugin) {
        this.plugin = plugin;
    }

    /**
     * Enables this add-on.
     *
     * @param config configuration of this add-on
     */
    public void enable(ConfigurationSection config) {
        Plugin dynmap = plugin.getServer().getPluginManager().getPlugin("dynmap");
        DynmapAPI api = (DynmapAPI) dynmap;
        MarkerAPI markerApi = api.getMarkerAPI();

        markerIcon = markerApi.getMarkerIcon(config.getString("marker"));
        ignoredIslands = new ArrayList<>();
        if (config.contains("ignoredIslands")) {
            for (Map map : config.getMapList("ignoredIslands")) {
                ignoredIslands.add(new PlotId((Integer) map.get("x"), (Integer) map.get("z"), (String) map.get("world")));
            }
        }

        markers = markerApi.getMarkerSet("skyblockplus.islands");
        if (markers == null) {
            markers = markerApi.createMarkerSet("skyblockplus.islands", "Islands", null, false);
        } else {
            markers.setMarkerSetLabel("Islands");
        }
        markers.setHideByDefault(false);

        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Updater(), 20 * 10);
    }

    private class Updater implements Runnable {
        @Override
        public void run() {
            Map<PlotId, Marker> newmap = new HashMap<>();

            for (SkyblockWorld world : SkyblockPlus.getWorlds()) {
                for (Plot plot : PlotPlus.getApi().getWorld(world.getName()).getPlots()) {
                    try {
                        handleIsland(plugin.getPersistence().getIsland(plot.getId()), newmap);
                    } catch (PersistenceException e) {
                        plugin.getLogger().log(Level.WARNING, "Could not get island: " + plot.getId(), e);
                    }
                }
            }

            //every marker that's not yet removed from plotAreas doesn't exist anymore (i.e. deleted islands)
            for (Marker oldm : islands.values()) {
                oldm.deleteMarker();
            }

            islands = newmap;

            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Updater(), 20 * 60 * 30); //30 minutes
        }

        private void handleIsland(Island island, Map<PlotId, Marker> newmap) {
            if (!ignoredIslands.contains(island.getId())) {
                Marker m = islands.get(island.getId());
                if (m != null) {
                    m.deleteMarker();
                }

                //create new marker
                PointXZ center = island.getCenter();
                m = markers.createMarker(island.getId().toString(), getLabel(island),
                        false, island.getWorld().getName(),
                        center.getX(), 125, center.getZ(),
                        markerIcon, false);

                if (m != null) {
                    newmap.put(island.getId(), m);
                }
            }
        }

        private String getLabel(Island island) {
            StringBuilder label = new StringBuilder();
            label.append(island.getLeader().getName()).append("'s island");

            List<SkyblockPlayer> team = Lists.newArrayList(island.getTeam());
            if (team.size() > 1) {
                label.append("<br/>Team: ");

                for (SkyblockPlayer player : team) {
                    if (!player.getUniqueId().equals(island.getLeader().getUniqueId())) {
                        label.append(player.getName()).append(", ");
                    }
                }

                label.setLength(label.length() - 2);
            }

            return label.toString();
        }
    }
}
