package de.craften.plugins.skyblockplus.addons;

import com.alecgorge.minecraft.jsonapi.JSONAPI;
import com.alecgorge.minecraft.jsonapi.api.APIMethodName;
import com.alecgorge.minecraft.jsonapi.api.JSONAPICallHandler;
import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.challenges.Challenge;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.json.simple.JSONObject;

import java.util.*;

/**
 * Add-on that adds request handlers to JSONAPI.
 */
public class JsonapiAddon {
    private final SkyblockPlus plugin;

    /**
     * Creates a new instance of this add-on.
     *
     * @param plugin plugin instance
     */
    public JsonapiAddon(SkyblockPlus plugin) {
        this.plugin = plugin;
    }


    /**
     * Enables this add-on.
     *
     * @param config configuration of this add-on
     */
    public void enable(ConfigurationSection config) {
        JSONAPI jsonapi = (JSONAPI) plugin.getServer().getPluginManager().getPlugin("JSONAPI");
        jsonapi.registerAPICallHandler(new CallHandler());
    }

    //TODO Refactor everything below this line...

    private class CallHandler implements JSONAPICallHandler {

        @Override
        public boolean willHandle(APIMethodName apiMethodName) {
            if (apiMethodName.getNamespace().equals("skyblockplus")) {
                switch (apiMethodName.getMethodName()) {
                    case "player":
                    case "ranking":
                    case "challenges":
                        return true;
                }
            }
            return false;
        }

        @Override
        public Object handle(APIMethodName apiMethodName, Object[] objects) {
            switch (apiMethodName.getMethodName()) {
                case "player":
                    return toJson(plugin.getPlayer(Bukkit.getOfflinePlayer(objects[0].toString())));
                case "ranking":
                    int offset = Integer.parseInt(objects[1].toString());
                    int count = Integer.parseInt(objects[2].toString());
                    return getRanking(objects[0].toString(), offset, count);
                case "challenges":
                    return toJson(SkyblockPlus.getWorld(objects[0].toString()).getChallenges());
            }
            return null;
        }

        private List<RankedIsland> getRanking(String world) {
            List<RankedIsland> ranking = new ArrayList<>();
            //retrieve islands
            for (Island island : SkyblockPlus.getWorld(world).getIslands()) {
                ranking.add(new RankedIsland(island));
            }

            //sort them by level
            Collections.sort(ranking, new Comparator<RankedIsland>() {
                @Override
                public int compare(RankedIsland a, RankedIsland b) {
                    return b.island.getLevel() - a.island.getLevel();
                }
            });

            //rank them
            int rank = 0;
            int cnt = 0;
            int level = Integer.MAX_VALUE;
            for (RankedIsland island : ranking) {
                cnt++;
                if (island.island.getLevel() < level) {
                    level = island.island.getLevel();
                    rank = cnt;
                }
                island.rank = rank;
            }

            return ranking;
        }


        private Object getRanking(String world, int offset, int count) {
            List<RankedIsland> ranking = getRanking(world);

            //return something that can be properly converted to json
            List<Object> jsonizableOutput = new ArrayList<>(count);
            for (RankedIsland island : ranking.subList(offset, Math.min(ranking.size(), count + offset))) {
                Map<String, Object> map = new HashMap<>(2);
                map.put("rank", island.rank);
                map.put("island", toJson(island.island));
                jsonizableOutput.add(map);
            }
            return jsonizableOutput;
        }

        private class RankedIsland {
            private final Island island;
            private int rank;

            public RankedIsland(Island island) {
                this.island = island;
            }
        }

        private Object toJson(Collection<Challenge> challenges) {
            HashMap<String, Object> result = new HashMap<>();
            for (Challenge challenge : challenges) {
                result.put(challenge.getName(), toJson(challenge));
            }
            return result;
        }

        private Object toJson(Challenge challenge) {
            Map<String, Object> result = new HashMap<>();
            result.put("displayName", challenge.getDisplayName());
            result.put("description", challenge.getDescription());
            result.put("difficulty", challenge.getDifficulty().toString()); //explicitly convert these values to strings to make sure the JSON is valid
            result.put("repeatable", Boolean.toString(challenge.isRepeatable()));
            return result;
        }

        private Object toJson(SkyblockPlayer player) {
            JSONObject worldsJson = new JSONObject();
            for (Map.Entry<String, Map<String, Integer>> world : player.getChallengeFinishedCounts().entrySet()) {
                JSONObject challengesJson = new JSONObject();

                for (Challenge challenge : plugin.getWorld(Bukkit.getWorld(world.getKey())).getChallenges()) {
                    JSONObject challengeObject = new JSONObject();
                    challengeObject.put("challenge", toJson(challenge));
                    Integer count = world.getValue().get(challenge.getName());
                    challengeObject.put("count", count == null ? 0 : count);

                    challengesJson.put(challenge.getName(), challengeObject);
                }

                worldsJson.put(world.getKey(), challengesJson);
            }

            Map<String, Object> result = new HashMap<>();
            result.put("islands", rankedIslandsToJson(addRankings(player.getIslands())));
            result.put("challenges", worldsJson);
            return result;
        }

        private List<RankedIsland> addRankings(List<Island> islands) {
            //TODO cache rankings
            List<RankedIsland> rankedIslands = new ArrayList<>(islands.size());

            Map<String, List<RankedIsland>> rankings = new HashMap<>();
            for (Island island : islands) {
                if (!rankings.containsKey(island.getWorld().getName())) {
                    rankings.put(island.getWorld().getName(), getRanking(island.getWorld().getName()));
                }
                for (RankedIsland rankedIsland : rankings.get(island.getWorld().getName())) {
                    if (rankedIsland.island.getId().equals(island.getId())) {
                        rankedIslands.add(rankedIsland);
                        break;
                    }
                }
            }

            return rankedIslands;
        }

        private List<Object> islandsToJson(List<Island> islands) {
            List<Object> result = new ArrayList<>();
            for (Island island : islands) {
                result.add(toJson(island));
            }
            return result;
        }

        private List<Object> rankedIslandsToJson(List<RankedIsland> islands) {
            List<Object> result = new ArrayList<>();
            for (RankedIsland island : islands) {
                result.add(toJson(island));
            }
            return result;
        }

        private Map<String, Object> toNameAndUuidJson(SkyblockPlayer player) {
            Map<String, Object> result = new HashMap<>(2);
            result.put("uuid", player.getUniqueId().toString());
            result.put("name", player.getName());
            return result;
        }

        private Map<String, Object> toJson(Island island) {
            Map<String, Object> result = new HashMap<>();

            PointXZ center = island.getCenter();
            result.put("x", center.getX());
            result.put("z", center.getZ());
            result.put("world", island.getWorld().getName());
            result.put("leader", toNameAndUuidJson(island.getLeader()));
            result.put("level", island.getLevel());

            List<Object> party = new ArrayList<>();
            for (SkyblockPlayer member : island.getTeam()) {
                party.add(toNameAndUuidJson(member));
            }
            result.put("party", party);
            return result;
        }

        private Object toJson(RankedIsland island) {
            Map<String, Object> result = toJson(island.island);
            result.put("rank", island.rank);
            return result;
        }
    }
}
