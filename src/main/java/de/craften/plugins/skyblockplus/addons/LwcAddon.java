package de.craften.plugins.skyblockplus.addons;

import com.griefcraft.lwc.LWC;
import com.griefcraft.model.Protection;
import com.griefcraft.scripting.event.LWCProtectionRegisterEvent;
import com.griefcraft.scripting.event.LWCProtectionRegistrationPostEvent;
import com.griefcraft.sql.PhysDB;
import de.craften.plugins.skyblockplus.skyblock.Island;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;

/**
 * Addon to use LWC for protecting the initial chest.
 */
public class LwcAddon {
    /**
     * Add a private protection for the given island's leader to the given chest.
     *
     * @param island island
     * @param chest  chest to protect
     */
    public static void addProtection(Island island, Chest chest) {
        resetProtections(chest); //ensure that there is no protection, yet

        Player player = island.getLeader().getBukkitPlayer();

        LWCProtectionRegisterEvent evt = new LWCProtectionRegisterEvent(player, chest.getBlock());
        LWC.getInstance().getModuleLoader().dispatchEvent(evt);

        // another plugin cancelled the registration
        if (evt.isCancelled()) {
            return;
        }
        PhysDB db = LWC.getInstance().getPhysicalDatabase();
        Protection protection = db.registerProtection(chest.getTypeId(), Protection.Type.PRIVATE,
                chest.getWorld().getName(), island.getLeader().getUniqueId().toString(), "",
                chest.getX(), chest.getY(), chest.getZ());

        protection.removeCache(); // Fix the blocks that match it
        LWC.getInstance().getProtectionCache().addProtection(protection);

        LWC.getInstance().getModuleLoader().dispatchEvent(new LWCProtectionRegistrationPostEvent(protection));
    }

    /**
     * Remove all protections of the given chest.
     *
     * @param chest chest to remove all protections of
     */
    public static void resetProtections(Chest chest) {
        Protection protection;
        while ((protection = LWC.getInstance().findProtection(chest.getBlock())) != null) {
            protection.remove();
        }
    }
}
