package de.craften.plugins.skyblockplus.challenges;

import de.craften.plugins.skyblockplus.challenges.rewards.Rewards;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import de.craften.plugins.skyblockplus.skyblock.SkyblockWorld;
import de.craften.plugins.skyblockplus.util.Item;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * A skyblock challenge.
 */
public class Challenge {
    private final String name;
    private final String displayName;
    private final String description;
    private final ChallengeDifficulty difficulty;
    private final Rewards rewards;
    private final Rewards repeatRewards;
    private final int order;
    private final Item icon;
    private final List<String> requiredChallenges;
    private final SkyblockWorld world;
    private final boolean isRepeatable;
    private final List<ChallengePart> subChallenges;

    private Challenge(String name, ConfigurationSection config, SkyblockWorld world, List<ChallengePart> subChallenges) {
        this.name = name;
        displayName = config.getString("displayName", name);
        description = config.getString("description", "");
        difficulty = ChallengeDifficulty.valueOf(config.getString("difficulty", "MEDIUM").toUpperCase());
        requiredChallenges = config.getStringList("requires.challenges"); //no need for null-check, see getRequiredChallenges()
        this.isRepeatable = config.getBoolean("isRepeatable", true);
        this.world = world;
        this.subChallenges = subChallenges;

        rewards = new Rewards(config.getConfigurationSection("rewards"));
        if (config.contains("repeatRewards")) {
            repeatRewards = new Rewards(config.getConfigurationSection("repeatRewards"));
        } else {
            repeatRewards = rewards;
        }

        order = config.getInt("order", 1);
        icon = new Item(config.getString("icon", "339"));
    }

    /**
     * Gets the name of this challenge.
     *
     * @return the name of this challenge
     * @see #getDisplayName() for getting a beautiful name
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the display name of this challenge.
     *
     * @return the display name of this challenge
     * @see #getName() for getting a unique challenge name
     */
    public final String getDisplayName() {
        return displayName;
    }

    /**
     * Gets the description of this challenge.
     *
     * @return the description of this challenge
     */
    public final String getDescription() {
        return description;
    }

    /**
     * Gets the difficulty of this challenge.
     *
     * @return the difficulty of this challenge
     */
    public final ChallengeDifficulty getDifficulty() {
        return difficulty;
    }

    /**
     * Gets the world of this challenge.
     *
     * @return the world of this challenge
     */
    public SkyblockWorld getWorld() {
        return world;
    }

    /**
     * Finishes this challenge and gives the rewards to the player.
     *
     * @param player player that finishes this challenge
     * @param island island to finish this challenge on
     * @return true if the challenge was successfully finished, false if it couldn't be finished yet
     */
    public final boolean finish(SkyblockPlayer player, Island island) {
        boolean firstTime = player.getChallengeFinishedCount(this) == 0;

        if ((firstTime || isRepeatable()) && canFinish(player, island)) {
            performFinish(player, island);
            if (firstTime) {
                rewards.giveTo(player);
            } else {
                repeatRewards.giveTo(player);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets a text representation of the rewards the given player will get when he finishes this challenge.
     *
     * @param player player to get the rewards text for
     * @return rewards text for the given player
     */
    public final String getRewardsText(SkyblockPlayer player) {
        if (player.getChallengeFinishedCount(this) == 0) {
            return rewards.getText();
        } else {
            return repeatRewards.getText();
        }
    }

    /**
     * Checks if this challenge can be finished by the given player on the given island.
     *
     * @param player player to finish this challenge with
     * @param island island to finish this challenge on
     * @return true if this challenge could be finished, false if not
     */
    protected boolean canFinish(SkyblockPlayer player, Island island) {
        for (ChallengePart challenge : subChallenges) {
            if (!challenge.canFinish(player, island)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Finishes this challenge with the given player on the given island.
     * <p/>
     * This method must not give any rewards nor send messages.
     *
     * @param player player to finish this challenge with
     * @param island island to finish this challenge on
     */
    protected void performFinish(SkyblockPlayer player, Island island) {
        for (ChallengePart challenge : subChallenges) {
            challenge.performFinish(player, island);
        }
    }

    /**
     * Checks if this challenge can be finished multiple times.
     *
     * @return true if this challenge can be finished multiple times, false if not
     */
    public final boolean isRepeatable() {
        return isRepeatable;
    }

    /**
     * Gets the order number of this challenge.
     *
     * @return order number
     */
    public int getOrder() {
        return order;
    }

    /**
     * Gets an icon for this challenge.
     *
     * @return an icon for this challenge
     */
    public Item getIcon() {
        return icon;
    }

    /**
     * Checks if the required items are taken when finishing this challenge.
     *
     * @return true if the required items are taken when finishing this challenge, false if not
     */
    public final boolean isTakingItems() {
        for (ChallengePart challenge : subChallenges) {
            if (challenge instanceof ItemsChallenge && ((ItemsChallenge) challenge).isTakingItems()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the required entities are removed when finishing this challenge.
     *
     * @return true if the required entities are removed when finishing this challenge, false if not
     */
    public final boolean isRemovingEntities() {
        for (ChallengePart challenge : subChallenges) {
            if (challenge instanceof IslandEntityChallenge && ((IslandEntityChallenge) challenge).isRemovingEntities()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets all required challenges to unlock this challenges.
     *
     * @return iterator for the required challenges
     */
    public Iterable<Challenge> getRequiredChallenges() {
        if (requiredChallenges == null) {
            return Collections.emptyList();
        }

        return new Iterable<Challenge>() {
            @Override
            public Iterator<Challenge> iterator() {
                return new Iterator<Challenge>() {
                    private int index = 0;

                    @Override
                    public boolean hasNext() {
                        return index < requiredChallenges.size();
                    }

                    @Override
                    public Challenge next() {
                        return world.getChallenge(requiredChallenges.get(index++));
                    }

                    @Override
                    public void remove() {
                    }
                };
            }
        };
    }

    /**
     * Checks if this challenge is locked for the given player.
     *
     * @param player player to check for
     * @return true if this challenge is locked for the given player, false if not
     */
    public boolean isLocked(SkyblockPlayer player) {
        for (Challenge challenge : getRequiredChallenges()) {
            if (player.getChallengeFinishedCount(challenge) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates a {@link de.craften.plugins.skyblockplus.challenges.Challenge} from the given configuration.
     *
     * @param name   the name of the challenge
     * @param config configuration to load the challenge form
     * @return challenge instance using the given configuration
     */
    public static Challenge fromConfig(String name, ConfigurationSection config, SkyblockWorld world) {
        List<ChallengePart> challenges = new ArrayList<>();

        if (config.contains("requires.islandLevel")) {
            challenges.add(new IslandLevelChallenge(config));
        }
        if (config.contains("requires.items")) {
            challenges.add(new ItemsChallenge(config));
        }
        if (config.contains("requires.blocks")) {
            challenges.add(new IslandBuildingChallenge(config));
        }
        if (config.contains("requires.entities")) {
            challenges.add(new IslandEntityChallenge(config));
        }
        if (config.contains("requires.permissions")) {
            challenges.add(new PermissionChallenge(config));
        }

        return new Challenge(name, config, world, challenges);
    }
}
