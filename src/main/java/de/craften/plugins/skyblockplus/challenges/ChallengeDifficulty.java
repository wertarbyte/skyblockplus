package de.craften.plugins.skyblockplus.challenges;

/**
 * A difficulty level of a {@link de.craften.plugins.skyblockplus.challenges.Challenge}.
 */
public enum ChallengeDifficulty {
    EASY,
    MEDIUM,
    HARD,
    EXPERT
}
