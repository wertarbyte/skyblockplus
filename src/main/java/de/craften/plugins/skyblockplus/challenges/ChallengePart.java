package de.craften.plugins.skyblockplus.challenges;

import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;

/**
 * A partial skyblock challenge.
 */
abstract class ChallengePart {
    /**
     * Checks if this challenge can be finished by the given player on the given island.
     *
     * @param player player to finish this challenge with
     * @param island island to finish this challenge on
     * @return true if this challenge could be finished, false if not
     */
    protected abstract boolean canFinish(SkyblockPlayer player, Island island);

    /**
     * Finishes this challenge with the given player on the given island.
     * <p/>
     * This method must not give any rewards nor send messages.
     *
     * @param player player to finish this challenge with
     * @param island island to finish this challenge on
     */
    protected abstract void performFinish(SkyblockPlayer player, Island island);
}
