package de.craften.plugins.skyblockplus.challenges;

import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import de.craften.plugins.skyblockplus.util.IgnoreDataItem;
import de.craften.plugins.skyblockplus.util.Item;
import de.craften.plugins.skyblockplus.util.ItemList;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;

/**
 * A challenge that requires some blocks to be built on an island.
 */
class IslandBuildingChallenge extends ChallengePart {
    private final ItemList requiredBlocks;

    IslandBuildingChallenge(ConfigurationSection config) {
        if (config.isString("requires.blocks")) {
            requiredBlocks = new ItemList(config.getString("requires.blocks", ""));
        } else {
            requiredBlocks = new ItemList();
            ConfigurationSection items = config.getConfigurationSection("requires.blocks");
            for (String key : items.getKeys(false)) {
                ConfigurationSection block = items.getConfigurationSection(key);
                if (block.contains("data")) {
                    requiredBlocks.add(new Item(block.getInt("id"), block.getInt("data")), block.getInt("count", 1));
                } else {
                    requiredBlocks.add(new IgnoreDataItem(block.getInt("id")), block.getInt("count", 1));
                }
            }
        }
    }

    @Override
    protected boolean canFinish(SkyblockPlayer player, Island island) {
        ItemList checklist = getRequiredBlocks();
        World world = island.getWorld().getBukkitWorld();

        for (PointXZ p : island.getRegion()) {
            for (int y = 0; y < world.getMaxHeight(); y++) {
                Block block = world.getBlockAt(p.getX(), y, p.getZ());
                checklist.remove(new Item(block.getTypeId(), block.getData()), 1);
            }

            if (checklist.isEmpty()) {
                return true;
            }
        }

        return checklist.isEmpty();
    }

    @Override
    protected void performFinish(SkyblockPlayer player, Island island) {
        //nothing to do
    }

    /**
     * Gets the required blocks to finish this challenge.
     *
     * @return the required blocks to finish this challenge
     */
    public ItemList getRequiredBlocks() {
        return new ItemList(requiredBlocks);
    }
}
