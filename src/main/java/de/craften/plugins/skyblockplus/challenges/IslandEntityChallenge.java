package de.craften.plugins.skyblockplus.challenges;

import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.plotplus.util.region.Region;
import de.craften.plugins.skyblockplus.challenges.matchers.EntityMatcher;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.Chunk;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A challenge that requires entities to be on the island.
 */
class IslandEntityChallenge extends ChallengePart {
    private final boolean removeEntities;
    private final List<EntityMatcher> requiredEntities;

    IslandEntityChallenge(ConfigurationSection config) {
        removeEntities = config.getBoolean("removeEntities", false);

        requiredEntities = new ArrayList<>();
        ConfigurationSection entities = config.getConfigurationSection("requires.entities");
        for (String key : entities.getKeys(false)) {
            requiredEntities.add(new EntityMatcher(entities.getConfigurationSection(key)));
        }

        Collections.sort(requiredEntities, new Comparator<EntityMatcher>() {
            @Override
            public int compare(EntityMatcher a, EntityMatcher b) {
                return b.getRelativeSpecialization() - a.getRelativeSpecialization();
            }
        });
    }

    @Override
    protected boolean canFinish(SkyblockPlayer player, Island island) {
        Region region = island.getRegion();
        int[] missingEntities = new int[requiredEntities.size()];
        int missingEntitiesSum = 0;

        for (int i = 0; i < missingEntities.length; i++) {
            missingEntities[i] = requiredEntities.get(i).getCount();
            missingEntitiesSum += missingEntities[i];
        }

        for (PointXZ chunkid : region.getInvolvedChunks()) {
            Chunk chunk = island.getWorld().getBukkitWorld().getChunkAt(chunkid.getX(), chunkid.getZ());
            for (Entity entity : chunk.getEntities()) {
                if (region.contains(entity.getLocation())) {
                    for (int i = 0; i < requiredEntities.size(); i++) {
                        if (missingEntities[i] > 0) {
                            if (requiredEntities.get(i).matches(entity)) {
                                missingEntities[i]--;
                                missingEntitiesSum--;
                                break;
                            }
                        }
                    }

                    if (missingEntitiesSum == 0) {
                        return true;
                    }
                }
            }
        }

        return missingEntitiesSum == 0;
    }

    @Override
    protected void performFinish(SkyblockPlayer player, Island island) {
        if (isRemovingEntities()) {
            Region region = island.getRegion();
            int[] missingEntities = new int[requiredEntities.size()];
            int missingEntitiesSum = 0;

            for (int i = 0; i < missingEntities.length; i++) {
                missingEntities[i] = requiredEntities.get(i).getCount();
                missingEntitiesSum += missingEntities[i];
            }

            for (PointXZ chunkid : region.getInvolvedChunks()) {
                Chunk chunk = island.getWorld().getBukkitWorld().getChunkAt(chunkid.getX(), chunkid.getZ());
                for (Entity entity : chunk.getEntities()) {
                    if (region.contains(entity.getLocation())) {
                        for (int i = 0; i < requiredEntities.size(); i++) {
                            if (missingEntities[i] > 0) {
                                if (requiredEntities.get(i).matches(entity)) {
                                    missingEntities[i]--;
                                    missingEntitiesSum--;
                                    entity.remove();
                                    break;
                                }
                            }
                        }

                        if (missingEntitiesSum == 0) {
                            return;
                        }
                    }
                }
            }
        }
    }

    /**
     * Checks if this challenges removes the required entities.
     *
     * @return true if this challenge removes the required entities, false if not
     */
    public boolean isRemovingEntities() {
        return removeEntities;
    }
}
