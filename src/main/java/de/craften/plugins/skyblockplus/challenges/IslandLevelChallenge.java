package de.craften.plugins.skyblockplus.challenges;

import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.configuration.ConfigurationSection;

/**
 * A challenge that requires a specific island level to finish.
 */
class IslandLevelChallenge extends ChallengePart {
    private final int requiredLevel;

    IslandLevelChallenge(ConfigurationSection config) {
        this.requiredLevel = config.getInt("requires.islandLevel");
    }

    /**
     * Gets the required island level to finish this challenge.
     *
     * @return required island level to finish this challenge
     */
    public final int getRequiredLevel() {
        return requiredLevel;
    }

    @Override
    protected boolean canFinish(SkyblockPlayer player, Island island) {
        if (island.getLevel() >= getRequiredLevel()) {
            return true;
        }

        island.recalculateLevel();
        return island.getLevel() >= getRequiredLevel();
    }

    @Override
    protected void performFinish(SkyblockPlayer player, Island island) {
        //nothing to do
    }
}
