package de.craften.plugins.skyblockplus.challenges;

import de.craften.plugins.skyblockplus.challenges.matchers.ItemMatcher;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A challenge that requires items to finish.
 */
class ItemsChallenge extends ChallengePart {
    private final boolean takeItems;
    private final List<ItemMatcher> requiredItems;

    ItemsChallenge(ConfigurationSection config) {
        takeItems = config.getBoolean("takeItems", true);

        if (config.isString("requires.items")) {
            requiredItems = ItemMatcher.fromString(config.getString("requires.items"));
        } else {
            requiredItems = new ArrayList<>();
            ConfigurationSection items = config.getConfigurationSection("requires.items");
            for (String key : items.getKeys(false)) {
                requiredItems.add(new ItemMatcher(items.getConfigurationSection(key)));
            }

            ((ArrayList) requiredItems).trimToSize();
        }

        //sort matchers by relative specialization (i.e. check for clownfish before checking for fish)
        Collections.sort(requiredItems, new Comparator<ItemMatcher>() {
            @Override
            public int compare(ItemMatcher a, ItemMatcher b) {
                return b.getRelativeSpecialization() - a.getRelativeSpecialization();
            }
        });
    }

    @Override
    protected boolean canFinish(SkyblockPlayer player, Island island) {
        ItemStack[] items = player.getBukkitPlayer().getInventory().getContents();
        int[] takenItems = new int[items.length];

        for (ItemMatcher matcher : this.requiredItems) { //we iterate over the checklist that we don't modify
            int missingItems = matcher.getCount();

            for (int i = 0; i < items.length; i++) {
                ItemStack slot = items[i];
                if (matcher.matches(slot)) {
                    if (missingItems > (slot.getAmount() - takenItems[i])) {
                        missingItems -= slot.getAmount() - takenItems[i];
                        takenItems[i] += slot.getAmount() - takenItems[i];
                    } else {
                        takenItems[i] += missingItems;
                        missingItems = 0;
                    }
                }
            }

            if (missingItems > 0) {
                return false;
            }
        }

        return true;
    }

    @Override
    protected void performFinish(SkyblockPlayer player, Island island) {
        if (isTakingItems()) {
            Inventory inventory = player.getBukkitPlayer().getInventory();
            ItemStack[] items = inventory.getContents();

            for (ItemMatcher matcher : this.requiredItems) { //we iterate over the checklist that we don't modify
                int missingItems = matcher.getCount();

                for (int i = 0; i < items.length; i++) {
                    ItemStack slot = items[i];
                    if (matcher.matches(slot)) {
                        if (missingItems >= slot.getAmount()) {
                            missingItems -= slot.getAmount();
                            items[i] = null;
                        } else {
                            slot.setAmount(slot.getAmount() - missingItems);
                            missingItems = 0;
                        }
                    }
                }
            }

            inventory.setContents(items);
        }
    }

    /**
     * Checks if the items are taken when finishing this challenge.
     *
     * @return true if the items are taken when finishing this challenge, false if not
     */
    public final boolean isTakingItems() {
        return takeItems;
    }
}
