package de.craften.plugins.skyblockplus.challenges;

import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.configuration.ConfigurationSection;

import java.util.List;

/**
 * A challenge that requires one or more permissions.
 */
public class PermissionChallenge extends ChallengePart {
    private final List<String> permissions;

    PermissionChallenge(ConfigurationSection config) {
        permissions = config.getStringList("requires.permissions");
    }

    @Override
    protected boolean canFinish(SkyblockPlayer player, Island island) {
        for (String permission : permissions) {
            if (!player.getBukkitPlayer().hasPermission(permission)) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void performFinish(SkyblockPlayer player, Island island) {
        //Nothing to do here
    }
}
