package de.craften.plugins.skyblockplus.challenges.matchers;

import de.craften.plugins.skyblockplus.challenges.matchers.properties.EntityName;
import de.craften.plugins.skyblockplus.challenges.matchers.properties.PropertyChecker;
import de.craften.plugins.skyblockplus.challenges.matchers.properties.WoolColor;
import org.bukkit.DyeColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

/**
 * A matcher for entities.
 */
public class EntityMatcher {
    private EntityType type;
    private int count;

    private List<PropertyChecker<Entity>> properties;

    public EntityMatcher(ConfigurationSection config) {
        type = EntityType.valueOf(config.getString("type"));
        count = config.getInt("count", 1);

        if (config.contains("name")) {
            properties = new ArrayList<>();
            properties.add(new EntityName(config.getString("name")));
        }

        if (config.contains("woolColor")) {
            if (properties == null) {
                properties = new ArrayList<>();
            }
            properties.add(new WoolColor(DyeColor.valueOf(config.getString("woolColor"))));
        }

        if (properties != null) {
            ((ArrayList) properties).trimToSize();
        }
    }

    public int getCount() {
        return count;
    }

    public boolean matches(Entity entity) {
        if (entity.getType() != type) {
            return false;
        }

        if (properties != null) {
            for (PropertyChecker<Entity> property : properties) {
                if (!property.matches(entity)) {
                    return false;
                }
            }
        }

        return true;
    }

    public int getRelativeSpecialization() {
        if (properties != null) {
            return properties.size();
        }

        return 0;
    }
}
