package de.craften.plugins.skyblockplus.challenges.matchers;

import de.craften.plugins.skyblockplus.challenges.matchers.properties.ItemEnchantments;
import de.craften.plugins.skyblockplus.challenges.matchers.properties.ItemName;
import de.craften.plugins.skyblockplus.challenges.matchers.properties.PropertyChecker;
import de.craften.plugins.skyblockplus.util.IgnoreDataItem;
import de.craften.plugins.skyblockplus.util.Item;
import de.craften.plugins.skyblockplus.util.ItemList;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A matcher for items.
 */
public class ItemMatcher {
    private int id;
    private Integer data;
    private int count;

    private List<PropertyChecker<ItemStack>> properties;

    public ItemMatcher(ConfigurationSection config) {
        id = config.getInt("id");
        data = config.contains("data") ? config.getInt("data") : null;
        count = config.getInt("count", 1);

        if (config.contains("name")) {
            properties = new ArrayList<>();
            properties.add(new ItemName(config.getString("name")));
        }

        if (config.contains("enchantments")) {
            if (properties == null) {
                properties = new ArrayList<>();
            }

            ItemEnchantments enchantments = new ItemEnchantments();
            for (String enchantment : config.getConfigurationSection("enchantments").getKeys(false)) {
                enchantments.addEnchantment(Enchantment.getByName(enchantment.toUpperCase()), config.getInt("enchantments." + enchantment));
            }
            properties.add(enchantments);
        }

        if (properties != null) {
            ((ArrayList) properties).trimToSize();
        }
    }

    private ItemMatcher(int id, Integer count) {
        this.id = id;
        this.count = count;
    }

    public ItemMatcher(int id, int data, Integer count) {
        this.id = id;
        this.data = data;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    /**
     * Checks if this matcher matches the given stack. This does not check if the stack contains enough items.
     *
     * @param stack stack to check
     * @return true if this matcher matches the stack
     */
    public boolean matches(ItemStack stack) {
        if (stack == null) {
            return false;
        }

        if (id != stack.getTypeId()) {
            return false;
        }

        if (data != null && data != stack.getData().getData()) {
            return false;
        }

        if (properties != null) {
            for (PropertyChecker<ItemStack> property : properties) {
                if (!property.matches(stack)) {
                    return false;
                }
            }
        }

        return true;
    }

    public int getRelativeSpecialization() {
        int specialization = 0;

        if (data != null) {
            specialization++;
        }

        if (properties != null) {
            specialization += properties.size();
        }

        return specialization;
    }

    public static List<ItemMatcher> fromString(String items) {
        ItemList list = new ItemList(items);
        List<ItemMatcher> matchers = new ArrayList<>(list.getItems().size());

        for (Map.Entry<Item, Integer> item : list.getItems()) {
            if (item.getKey() instanceof IgnoreDataItem) {
                matchers.add(new ItemMatcher(item.getKey().getId(), item.getValue()));
            } else {
                matchers.add(new ItemMatcher(item.getKey().getId(), item.getKey().getData(), item.getValue()));
            }
        }

        return matchers;
    }
}
