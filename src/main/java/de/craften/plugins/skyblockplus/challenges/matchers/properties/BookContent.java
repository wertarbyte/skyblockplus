package de.craften.plugins.skyblockplus.challenges.matchers.properties;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.List;

/**
 * Setter for book metadata.
 */
public class BookContent implements PropertySetter<ItemStack> {
    private final String title;
    private final String author;
    private final List<String> pages;

    public BookContent(String title, String author, List<String> pages) {
        this.title = title;
        this.author = author;
        this.pages = pages;
    }

    @Override
    public void applyOn(ItemStack object) {
        if (object.getType() == Material.WRITTEN_BOOK || object.getType() == Material.BOOK_AND_QUILL) {
            BookMeta meta = (BookMeta) object.getItemMeta();
            meta.setTitle(title);
            meta.setAuthor(author);
            for (String page : pages) {
                meta.addPage(page);
            }
            object.setItemMeta(meta);
        }
    }
}
