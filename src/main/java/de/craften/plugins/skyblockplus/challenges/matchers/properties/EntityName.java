package de.craften.plugins.skyblockplus.challenges.matchers.properties;

import org.bukkit.entity.Entity;

public class EntityName implements PropertyChecker<Entity> {
    private final String name;

    public EntityName(String name) {
        this.name = name;
    }

    @Override
    public boolean matches(Entity entity) {
        return entity.getCustomName().equals(name);
    }
}
