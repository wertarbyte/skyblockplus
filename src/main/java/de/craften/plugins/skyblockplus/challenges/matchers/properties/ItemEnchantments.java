package de.craften.plugins.skyblockplus.challenges.matchers.properties;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class ItemEnchantments implements PropertyChecker<ItemStack>, PropertySetter<ItemStack> {
    private Map<Enchantment, Integer> enchantments = new HashMap<>();

    public void addEnchantment(Enchantment enchantment, int level) {
        enchantments.put(enchantment, level);
    }

    @Override
    public boolean matches(ItemStack object) {
        for (Map.Entry<Enchantment, Integer> enchantment : enchantments.entrySet()) {
            if (object.getEnchantmentLevel(enchantment.getKey()) < enchantment.getValue()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void applyOn(ItemStack object) {
        object.addEnchantments(enchantments);
    }
}
