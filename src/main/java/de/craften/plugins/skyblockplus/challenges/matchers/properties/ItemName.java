package de.craften.plugins.skyblockplus.challenges.matchers.properties;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemName implements PropertyChecker<ItemStack>, PropertySetter<ItemStack> {
    private final String name;

    public ItemName(String name) {
        this.name = name;
    }

    @Override
    public boolean matches(ItemStack object) {
        return name.equals(object.getItemMeta().getDisplayName());
    }

    @Override
    public void applyOn(ItemStack object) {
        ItemMeta meta = object.getItemMeta();
        meta.setDisplayName(name);
        object.setItemMeta(meta);
    }
}
