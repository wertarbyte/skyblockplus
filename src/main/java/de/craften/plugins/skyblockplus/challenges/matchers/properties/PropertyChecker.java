package de.craften.plugins.skyblockplus.challenges.matchers.properties;

/**
 * A checker for properties.
 */
public interface PropertyChecker<T> {
    boolean matches(T object);
}
