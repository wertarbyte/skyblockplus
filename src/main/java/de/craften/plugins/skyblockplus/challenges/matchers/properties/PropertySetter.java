package de.craften.plugins.skyblockplus.challenges.matchers.properties;

public interface PropertySetter<T> {
    void applyOn(T object);
}
