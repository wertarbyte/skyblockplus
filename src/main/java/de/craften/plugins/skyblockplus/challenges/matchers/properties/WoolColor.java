package de.craften.plugins.skyblockplus.challenges.matchers.properties;

import org.bukkit.DyeColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Sheep;

public class WoolColor implements PropertyChecker<Entity> {
    private final DyeColor color;

    public WoolColor(DyeColor color) {
        this.color = color;
    }

    @Override
    public boolean matches(Entity entity) {
        return entity instanceof Sheep && ((Sheep) entity).getColor().equals(color);
    }
}
