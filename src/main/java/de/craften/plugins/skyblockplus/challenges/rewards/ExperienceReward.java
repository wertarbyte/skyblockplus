package de.craften.plugins.skyblockplus.challenges.rewards;

import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;

/**
 * A reward that gives some experience points to a player.
 */
class ExperienceReward extends Reward {
    private int exp;

    public ExperienceReward(int exp) {
        this.exp = exp;
    }

    @Override
    public void giveTo(SkyblockPlayer player) {
        player.getBukkitPlayer().giveExp(exp);
    }
}
