package de.craften.plugins.skyblockplus.challenges.rewards;

import de.craften.plugins.skyblockplus.challenges.matchers.properties.BookContent;
import de.craften.plugins.skyblockplus.challenges.matchers.properties.ItemEnchantments;
import de.craften.plugins.skyblockplus.challenges.matchers.properties.ItemName;
import de.craften.plugins.skyblockplus.challenges.matchers.properties.PropertySetter;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import de.craften.plugins.skyblockplus.util.Item;
import de.craften.plugins.skyblockplus.util.ItemList;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A reward that gives items to a player.
 */
class ItemReward extends Reward {
    private int id;
    private int data;
    private int count;

    private List<PropertySetter<ItemStack>> properties;

    public ItemReward(int id, int data, int count) {
        this.id = id;
        this.data = data;
        this.count = count;
    }

    public ItemReward(ConfigurationSection config) {
        this(config.getInt("id"), config.getInt("data", 0), config.getInt("count", 1));

        if (config.contains("name")) {
            properties = new ArrayList<>();
            properties.add(new ItemName(config.getString("name")));
        }
        if (config.contains("enchantments")) {
            if (properties == null) {
                properties = new ArrayList<>();
            }

            ItemEnchantments enchantments = new ItemEnchantments();
            for (String enchantment : config.getConfigurationSection("enchantments").getKeys(false)) {
                enchantments.addEnchantment(Enchantment.getByName(enchantment.toUpperCase()), config.getInt("enchantments." + enchantment));
            }
            properties.add(enchantments);
        }

        if (config.contains("bookContent")) {
            if (properties == null) {
                properties = new ArrayList<>();
            }

            properties.add(new BookContent(
                    config.getString("bookContent.title", "Untitled book"),
                    config.getString("bookContent.author", "Herobrine"),
                    config.getStringList("bookContent.pages")));
        }

        if (properties != null) {
            ((ArrayList) properties).trimToSize();
        }
    }

    @Override
    public void giveTo(SkyblockPlayer player) {
        ItemStack itemStack = new ItemStack(id, count, (short) 0, (byte) data);

        if (properties != null) {
            for (PropertySetter<ItemStack> property : properties) {
                property.applyOn(itemStack);
            }
        }

        player.getBukkitPlayer().getInventory().addItem(itemStack);
    }

    public static List<Reward> fromString(String items) {
        ItemList list = new ItemList(items);
        List<Reward> rewards = new ArrayList<>(list.getItems().size());

        for (Map.Entry<Item, Integer> item : list.getItems()) {
            rewards.add(new ItemReward(item.getKey().getId(), item.getKey().getData(), item.getValue()));
        }

        return rewards;
    }
}
