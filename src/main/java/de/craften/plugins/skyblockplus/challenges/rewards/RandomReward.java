package de.craften.plugins.skyblockplus.challenges.rewards;

import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * A random reward.
 */
public class RandomReward extends Reward {
    private final List<ProbabilityRewards> rewards;
    private final int probablitySum;

    /**
     * Creates a new random reward.
     *
     * @param configurationSection configuration of the possible reward, a map of rewards
     */
    public RandomReward(ConfigurationSection configurationSection) {
        rewards = new ArrayList<>();
        int probabilitySum = 0;

        for (String rewardsKey : configurationSection.getKeys(false)) {
            ProbabilityRewards reward = new ProbabilityRewards(configurationSection.getConfigurationSection(rewardsKey));
            probabilitySum += reward.getRelativeProbability();
            this.rewards.add(reward);
        }

        ((ArrayList) rewards).trimToSize();
        this.probablitySum = probabilitySum;
    }

    /**
     * Gives one of the possible rewards to a player.
     *
     * @param player player to give a reward to
     */
    @Override
    public void giveTo(SkyblockPlayer player) {
        int random = ThreadLocalRandom.current().nextInt(probablitySum);
        int sum = 0;
        int i = 0;
        while (sum < random) {
            sum = sum + rewards.get(i).getRelativeProbability();
            i++;
        }
        rewards.get(Math.max(0, i - 1)).giveTo(player);
    }

    private class ProbabilityRewards extends Rewards {
        private final int probability;

        public ProbabilityRewards(ConfigurationSection config) {
            super(config);
            this.probability = config.getInt("probability", 1);
        }

        public int getRelativeProbability() {
            return probability;
        }
    }
}
