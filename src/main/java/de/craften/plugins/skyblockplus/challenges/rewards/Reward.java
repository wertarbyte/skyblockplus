package de.craften.plugins.skyblockplus.challenges.rewards;

import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import de.craften.plugins.skyblockplus.util.Item;
import de.craften.plugins.skyblockplus.util.ItemList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A reward.
 */
abstract class Reward {
    /**
     * Gives this reward to a player.
     *
     * @param player player to give this reward to
     */
    public abstract void giveTo(SkyblockPlayer player);
}
