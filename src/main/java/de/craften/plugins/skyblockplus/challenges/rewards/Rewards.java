package de.craften.plugins.skyblockplus.challenges.rewards;

import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A list of rewards with a text.
 */
public class Rewards {
    private final List<Reward> rewards;
    private final String text;

    public Rewards(ConfigurationSection config) {
        rewards = new ArrayList<>();

        if (config != null) {
            for (String rewardKey : config.getKeys(false)) {
                if (rewardKey.equals("items")) {
                    if (config.isString("items")) {
                        rewards.addAll(ItemReward.fromString(config.getString("items")));
                    } else {
                        for (String itemsKey : config.getConfigurationSection("items").getKeys(false)) {
                            rewards.add(new ItemReward(config.getConfigurationSection("items." + itemsKey)));
                        }
                    }
                } else if (rewardKey.equals("exp")) {
                    rewards.add(new ExperienceReward(config.getInt("exp")));
                } else if (rewardKey.contains("oneRandomOf")) {
                    rewards.add(new RandomReward(config.getConfigurationSection("oneRandomOf")));
                }
            }

            text = config.getString("text", "");
        } else {
            text = "";
        }
    }

    public String getText() {
        return text;
    }

    public void giveTo(SkyblockPlayer player) {
        for (Reward reward : rewards) {
            reward.giveTo(player);
        }
    }
}
