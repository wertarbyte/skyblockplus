package de.craften.plugins.skyblockplus.commands;

import de.craften.plugins.plotplus.util.commands.Command;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.commands.util.SkyblockCommandHandler;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockWorld;
import org.bukkit.command.CommandSender;

/**
 * Admin commands.
 */
public class AdminCommands extends SkyblockCommandHandler {
    public AdminCommands(SkyblockPlus plugin) {
        super(plugin);
    }

    @Command(value = "updatelevels", allowFromConsole = true, permission = "skyblock.adminutil", description = "Recalculate the levels of all islands.")
    public void updateLevels(final CommandSender commandSender) {
        long i = 0;
        for (SkyblockWorld world : SkyblockPlus.getWorlds()) {
            for (final Island island : world.getIslands()) {
                getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        commandSender.sendMessage("Recalculating island level for " + island);
                        island.recalculateLevel();
                    }
                }, 20 * i);
                i++;
            }
        }
    }

    @Command(value = "reload", allowFromConsole = true, permission = "skyblock.adminutil", description = "Reload the configuration, including worlds and challenges.")
    public void reloadChallenges(CommandSender commandSender) {
        getPlugin().reloadConfig();
        commandSender.sendMessage("Config reloaded");
    }
}
