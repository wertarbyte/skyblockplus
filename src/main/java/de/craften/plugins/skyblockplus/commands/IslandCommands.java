package de.craften.plugins.skyblockplus.commands;

import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.util.commands.Command;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.commands.util.SkyblockCommandHandler;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.util.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.logging.Level;

/**
 * Handler for island commands.
 */
public class IslandCommands extends SkyblockCommandHandler {
    public IslandCommands(SkyblockPlus plugin) {
        super(plugin);
    }

    @Command(description = "Open the SkyblockPlus main menu.")
    public void showGui(Player player) {
        getPlugin().getGui().showMenu(player);
    }

    @Command(value = "list", description = "Show a list of all your islands.")
    public void showList(Player player) {
        getPlugin().getGui().showIslandsMenu(player);
    }

    @Command(value = {"challenges", "c"}, description = "Show the challenge menu.")
    public void showChallenges(Player player) {
        Island island = getPlugin().getIslandAt(player);
        if (island != null) {
            if (island.mayBuild(player)) {
                getPlugin().getGui().showChallengeMenu(player, island);
            } else {
                player.sendMessage("You can't finish challenges on this island.");
            }
        } else {
            player.sendMessage("You are not on an island.");
        }
    }

    @Command(value = "biome", description = "Show a menu to change the biome of the island.")
    public void changeBiome(Player player) {
        Island island = getPlugin().getIslandAt(player);
        if (island != null) {
            if (island.mayBuild(player) || player.hasPermission("skyblock.island.manageany")) {
                getPlugin().getGui().showBiomeMenu(player, island);
            } else {
                player.sendMessage("You can't change the biome of this island.");
            }
        } else {
            player.sendMessage("You are not on an island.");
        }
    }

    @Command(value = "team", min = 2, max = 2, description = "Add and remove team members.", usage = "team add/remove <player>")
    public void manageTeam(Player player, String[] args) {
        Island island = getPlugin().getIslandAt(player);
        if (island != null) {
            if (args[0].equals("add")) {
                if (island.mayManage(player) || player.hasPermission("skyblock.island.manageany")) {
                    OfflinePlayer toAdd = Bukkit.getOfflinePlayer(args[1]);
                    try {
                        island.addTeamMember(getPlugin().getPlayer(toAdd));
                        player.sendMessage(toAdd.getName() + " was added to the team.");
                    } catch (IOException e) {
                        player.sendMessage(toAdd.getName() + " could not be added to the team.");
                    }
                } else {
                    player.sendMessage("You can't edit this team.");
                }
            } else if (args[0].equals("remove")) {
                if (island.mayManage(player) || player.hasPermission("skyblock.island.manageany")) {
                    OfflinePlayer toRemove = Bukkit.getOfflinePlayer(args[1]);
                    if (island.mayBuild(toRemove.getUniqueId())) {
                        if (island.getLeader().getUniqueId().equals(toRemove.getUniqueId())) {
                            player.sendMessage("The leader of an island can't be removed from the team.");
                        } else {
                            try {
                                island.removeTeamMember(getPlugin().getPlayer(toRemove));
                                player.sendMessage(toRemove.getName() + " was removed from the team.");
                            } catch (IOException e) {
                                player.sendMessage(toRemove.getName() + " could not be removed from the team.");
                            }
                        }
                    } else {
                        player.sendMessage(toRemove.getName() + " is not in the team.");
                    }
                } else {
                    player.sendMessage("You can't edit this team.");
                }
            }
        } else {
            player.sendMessage("You are not on an island.");
        }
    }

    @Command(value = "remove", min = 0, max = 1, description = "Remove the island.")
    public void removeIsland(Player player, String[] args) {
        Island island = getPlugin().getIslandAt(player);
        if (island != null) {
            if ((island.mayManage(player) && player.hasPermission("skyblock.island.remove"))
                    || player.hasPermission("skyblock.island.remove.any")) {
                if (args.length == 1 && args[0].equalsIgnoreCase("iKnowThatThisWillDeleteTheIsland")) {
                    try {
                        island.remove();
                        player.teleport(player.getWorld().getSpawnLocation());
                        player.sendMessage("Island removed.");
                    } catch (IOException e) {
                        getPlugin().getLogger().log(Level.SEVERE, "Could not remove island", e);
                        player.sendMessage("The island could not be removed.");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "You are about to delete your island! This can't be undone.");
                    player.sendMessage(ChatColor.RED + "Use " + ChatColor.GOLD + "/is remove iKnowThatThisWillDeleteTheIsland" +
                            ChatColor.RED + " to continue.");
                }
            } else {
                player.sendMessage("You have no permission to remove this island.");
            }
        } else {
            player.sendMessage("You are not on an island.");
        }
    }

    @Command(value = "restart", min = 0, max = 1, description = "Restart the island.")
    public void restartIsland(Player player, String[] args) {
        Island island = getPlugin().getIslandAt(player);
        if (island != null) {
            if ((island.mayManage(player) && player.hasPermission("skyblock.island.restart"))
                    || player.hasPermission("skyblock.island.restart.any")) {
                if (args.length == 1 && args[0].equalsIgnoreCase("iKnowThatThisWillRestartTheIsland")) {
                    island.restart();
                    player.teleport(island.getHomeLocation());
                    player.sendMessage("Island restarted.");
                } else {
                    player.sendMessage(ChatColor.RED + "You are about to restart your island! This can't be undone.");
                    player.sendMessage(ChatColor.RED + "Use " + ChatColor.GOLD + "/is restart iKnowThatThisWillRestartTheIsland" +
                            ChatColor.RED + " to continue.");
                }
            } else {
                player.sendMessage("You have no permission to restart this island.");
            }
        } else {
            player.sendMessage("You are not on an island.");
        }
    }

    @Command(value = "rename", min = 0, max = Integer.MAX_VALUE, description = "Rename the island.", usage = "rename <name>")
    public void renameIsland(Player player, String[] args) {
        Island island = getPlugin().getIslandAt(player);
        if (island != null) {
            if ((island.mayManage(player) && player.hasPermission("skyblock.island.rename"))
                    || player.hasPermission("skyblock.island.rename.any")) {
                try {
                    if (args.length > 0) {
                        island.setName(StringUtil.join(args, " "));
                        player.sendMessage("Renamed this island to \"" + island.getName() + "\".");
                    } else {
                       island.setName(null);
                        player.sendMessage("Removed the name of this island.");
                    }
                } catch (IOException e) {
                    getPlugin().getLogger().log(Level.WARNING, "Could not rename plot", e);
                    player.sendMessage("Renaming this island failed.");
                }
            } else {
                player.sendMessage("You have no permission to rename this island.");
            }
        } else {
            player.sendMessage("You are not on an island.");
        }
    }
}
