package de.craften.plugins.skyblockplus.commands;

import com.google.common.collect.Lists;
import de.craften.plugins.plotplus.util.commands.Command;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.commands.util.SkyblockCommandHandler;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockWorld;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Handler for teleport and warp commands.
 */
public class TeleportCommands extends SkyblockCommandHandler {
    public TeleportCommands(SkyblockPlus plugin) {
        super(plugin);
    }

    @Command(value = {"home", "h"}, description = "Teleport yourself to your island.")
    public void home(Player player) {
        SkyblockWorld skyblockWorld = SkyblockPlus.getWorld(player.getWorld().getName());
        List<Island> islands;
        if (skyblockWorld != null) {
            islands = Lists.newArrayList(skyblockWorld.getIslandsOf(getPlugin().getPlayer(player)));
        } else {
            islands = getPlugin().getPlayer(player).getIslands();
        }
        if (islands.isEmpty()) {
            player.sendMessage("You don't have an island.");
        } else if (islands.size() == 1) {
            player.teleport(islands.get(0).getHomeLocation());
        } else {
            getPlugin().getGui().showIslandsMenu(player);
        }
    }

    @Command(value = "warp", description = "Warp to the island of another player.", min = 1, max = 1, permission = "skyblock.island.warp", usage = "warp <player>")
    public void warp(Player player, String[] args) {
        OfflinePlayer warpToPlayer = Bukkit.getOfflinePlayer(args[0]);
        List<Island> islands;
        if (player.hasPermission("skyblock.island.warp.any")) {
            islands = getPlugin().getPlayer(warpToPlayer).getIslands();
        } else {
            islands = getPlugin().getPlayer(warpToPlayer).getWarpableIslands();
        }
        if (islands.isEmpty()) {
            player.sendMessage(warpToPlayer.getName() + " has no islands you can warp to.");
        } else if (islands.size() == 1) {
            player.teleport(islands.get(0).getWarpLocation());
        } else {
            getPlugin().getGui().showWarpMenu(warpToPlayer, islands, player);
        }
    }
}
