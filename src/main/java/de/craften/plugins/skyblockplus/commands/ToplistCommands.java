package de.craften.plugins.skyblockplus.commands;

import de.craften.plugins.mcguilib.text.TextBuilder;
import de.craften.plugins.plotplus.util.commands.Command;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.commands.util.SkyblockCommandHandler;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Handler for toplist commands.
 */
public class ToplistCommands extends SkyblockCommandHandler {
    public ToplistCommands(SkyblockPlus plugin) {
        super(plugin);
    }

    @Command(value = "top", description = "Show the top 10 islands.")
    public void showToplist(Player player) {
        List<Island> islands;
        try {
            islands = getPlugin().getPersistence().getAllIslands(player.getWorld().getName());
        } catch (de.craften.plugins.skyblockplus.persistence.PersistenceException e) {
            player.sendMessage("Could not get top list.");
            return;
        }
        Collections.sort(islands, new Comparator<Island>() {
            @Override
            public int compare(Island a, Island b) {
                return b.getLevel() - a.getLevel();
            }
        });
        for (int i = 0; i < 10 && islands.size() > i; i++) {
            Island island = islands.get(i);
            TextBuilder msg = TextBuilder.create((i + 1) + ". ").green().append(island.getLeader().getName());
            for (SkyblockPlayer p : island.getTeam()) {
                msg.append(", ").append(p.getName());
            }
            msg.append(" (Level ").green().append(island.getLevel() + ")").green();
            msg.sendTo(player);
        }
    }
}
