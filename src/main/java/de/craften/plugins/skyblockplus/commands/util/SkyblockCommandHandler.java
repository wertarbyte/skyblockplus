package de.craften.plugins.skyblockplus.commands.util;

import de.craften.plugins.plotplus.util.commands.CommandHandler;
import de.craften.plugins.skyblockplus.SkyblockPlus;

/**
 * A command handler for SkyblockPlus.
 */
public class SkyblockCommandHandler implements CommandHandler {
    private final SkyblockPlus plugin;

    public SkyblockCommandHandler(SkyblockPlus plugin) {
        this.plugin = plugin;
    }

    protected final SkyblockPlus getPlugin() {
        return plugin;
    }
}
