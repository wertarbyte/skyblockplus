package de.craften.plugins.skyblockplus.commands.util;

import de.craften.plugins.mcguilib.text.TextBuilder;
import de.craften.plugins.plotplus.util.commands.SubCommandHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Command handler for all skyblock commands.
 */
public class SkyblockSubCommandHandler extends SubCommandHandler {
    public SkyblockSubCommandHandler() {
        super("is");
    }

    @Override
    protected void onInvalidCommand(CommandSender commandSender) {
        TextBuilder.create("Unknown command. Use ").red()
                .append("/is help").white()
                .append(" to get a list of available commands.").red()
                .sendTo(commandSender);
    }

    @Override
    protected void onPermissionDenied(CommandSender commandSender, Command command, String[] strings) {
        TextBuilder.create("You don't have permission to use this command.").red().sendTo(commandSender);
    }

    @Override
    protected void sendHelpLine(CommandSender sender, de.craften.plugins.plotplus.util.commands.Command command) {
        if (command.permission().isEmpty() || sender.hasPermission(command.permission())) {
            TextBuilder msg = TextBuilder.create();
            msg.append("/is ").green();
            if (command.usage().length > 0) {
                msg.append(command.usage()[0]).green().append(" ");
            } else {
                if (command.value().length > 0) {
                    msg.append(command.value()[0]).green().append(" ");
                }
            }
            msg.append("- ").append(command.description());
            msg.sendTo(sender);
        }
    }
}
