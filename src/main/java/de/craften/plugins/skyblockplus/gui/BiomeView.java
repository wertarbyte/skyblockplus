package de.craften.plugins.skyblockplus.gui;

import de.craften.plugins.mcguilib.Button;
import de.craften.plugins.mcguilib.ClickListener;
import de.craften.plugins.mcguilib.SinglePageView;
import de.craften.plugins.mcguilib.text.TextBuilder;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.util.Item;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A view to edit an island's biome.
 */
class BiomeView extends SinglePageView {
    private static final Biome[] AVAILABLE_BIOMES = new Biome[]{
            Biome.PLAINS, Biome.OCEAN, Biome.FOREST, Biome.DESERT, Biome.JUNGLE, Biome.SWAMPLAND, Biome.TAIGA,
            Biome.MUSHROOM_ISLAND, Biome.HELL, Biome.SKY, Biome.SAVANNA, Biome.MESA};
    private static final Map<Biome, List<String>> BIOME_DESCRIPTIONS;

    private final Island island;

    static {
        BIOME_DESCRIPTIONS = new HashMap<>();
        BIOME_DESCRIPTIONS.put(Biome.PLAINS, TextBuilder.create()
                .append("The plains biome is the basic starting biome of all islands. Passive mobs (including horses) ").white()
                .append("and hostile mobs will spawn.").white()
                .getLines());
        BIOME_DESCRIPTIONS.put(Biome.OCEAN, TextBuilder.create()
                .append("The ocean biome prevents passive mobs like animals from spawning. Hostile mobs will spawn normally.").white()
                .getLines());
        BIOME_DESCRIPTIONS.put(Biome.FOREST, TextBuilder.create()
                .append("The forest biome allows animals and hostile mobs to spawn.").white()
                .getLines());
        BIOME_DESCRIPTIONS.put(Biome.DESERT, TextBuilder.create()
                .append("The desert biome disables rain and snow on your island. Passive mobs like animals won't spawn. ").white()
                .append("Hostile mobs will spawn normally.").white()
                .getLines());
        BIOME_DESCRIPTIONS.put(Biome.JUNGLE, TextBuilder.create()
                .append("The jungle biome is bright and colorful. Passive mobs (including ocelots) and hostile mobs will spawn.").white()
                .getLines());
        BIOME_DESCRIPTIONS.put(Biome.SWAMPLAND, TextBuilder.create()
                .append("The swamp biome is dark and dull. Passive mobs will spawn normally and slimes have a small chance ").white()
                .append("to spawn at night, depending on the moon phase.").white()
                .getLines());
        BIOME_DESCRIPTIONS.put(Biome.TAIGA, TextBuilder.create()
                .append("The taiga biome has snow instead of rain. Passive mobs (including wolves) and ").white()
                .append("hostile mobs will spawn.").white()
                .getLines());
        BIOME_DESCRIPTIONS.put(Biome.MUSHROOM_ISLAND, TextBuilder.create()
                .append("The mushroom biome is bright and colorful. Mooshrooms are the only mobs that will spawn.").white()
                .getLines());
        BIOME_DESCRIPTIONS.put(Biome.HELL, TextBuilder.create()
                .append("The nether biome looks dark and dead. Some mobs from the nether will ").white()
                .append("spawn in this biome (excluding ghasts and blazes).").white()
                .getLines());
        BIOME_DESCRIPTIONS.put(Biome.SKY, TextBuilder.create()
                .append("The sky biome gives your island a special dark sky. Only endermen will spawn in this biome.")
                .white().getLines());
        BIOME_DESCRIPTIONS.put(Biome.SAVANNA, TextBuilder.create()
                .append("The savanna biome is dry but a little less hot than the desert biome. No rain ").white()
                .append("will fall in this biome. Horses will spawn in this biome.")
                .white().getLines());
        BIOME_DESCRIPTIONS.put(Biome.MESA, TextBuilder.create()
                .append("The mesa biome is pretty dry. Passive and hostile mobs will spawn normally.")
                .white().getLines());
    }

    public BiomeView(final Island island) {
        super(ChatColor.DARK_BLUE + "Island Biome", 18);
        this.island = island;

        Button backButton = new Button(Material.SIGN, "Back");
        backButton.setOnClick(new ClickListener() {
            public void clicked(InventoryClickEvent event) {
                SkyblockGui.getInstance().showMenu(getViewer());
            }
        });
        addElement(backButton);

        for (Biome biome : AVAILABLE_BIOMES) {
            addElement(new BiomeButton(biome));
        }
    }

    private class BiomeButton extends Button {
        public BiomeButton(final Biome biome) {
            super(getIcon(biome).getMaterial(), getIcon(biome).getData(), ChatColor.BOLD + biome.name());
            setDescription(BIOME_DESCRIPTIONS.get(biome));
            setOnClick(new ClickListener() {
                @Override
                public void clicked(InventoryClickEvent inventoryClickEvent) {
                    if (System.currentTimeMillis() - island.getBiomeChangedTime() > 1000 * 60 * 30) { //30 min
                        island.setBiome(biome);
                        close();
                    } else {
                        getViewer().sendMessage("The biome of an island can only be changed once every 30 minutes.");
                    }
                }
            });
        }
    }

    /**
     * Gets the icon for the given biome.
     *
     * @param biome biome to get the icon for
     * @return icon for the given biome
     */
    public static Item getIcon(Biome biome) {
        switch (biome) {
            case PLAINS:
                return new Item(2, 0);
            case OCEAN:
                return new Item(326, 0);
            case FOREST:
                return new Item(6, 1);
            case DESERT:
                return new Item(12, 0);
            case JUNGLE:
                return new Item(6, 3);
            case SWAMPLAND:
                return new Item(111, 0);
            case TAIGA:
                return new Item(78, 0);
            case MUSHROOM_ISLAND:
            case MUSHROOM_SHORE:
                return new Item(40, 0);
            case HELL:
                return new Item(327, 0);
            case SKY:
                return new Item(381, 0);
            case SAVANNA:
                return new Item(6, 4);
            case MESA:
                return new Item(12, 1);
            default:
                return new Item(6, 0);
        }
    }
}
