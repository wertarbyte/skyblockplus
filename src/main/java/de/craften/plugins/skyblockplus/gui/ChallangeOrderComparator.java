package de.craften.plugins.skyblockplus.gui;

import de.craften.plugins.skyblockplus.challenges.Challenge;

import java.util.Comparator;

/**
 * Compares challanges by their order.
 */
class ChallangeOrderComparator implements Comparator<Challenge> {
    @Override
    public int compare(Challenge a, Challenge b) {
        return a.getOrder() - b.getOrder();
    }
}
