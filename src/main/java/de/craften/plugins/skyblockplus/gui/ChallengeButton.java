package de.craften.plugins.skyblockplus.gui;

import de.craften.plugins.mcguilib.Button;
import de.craften.plugins.mcguilib.ClickListener;
import de.craften.plugins.mcguilib.text.TextBuilder;
import de.craften.plugins.skyblockplus.challenges.Challenge;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * A button for a challenge.
 */
class ChallengeButton extends Button {
    private Challenge challenge;
    private Island island;
    private SkyblockPlayer player;

    public ChallengeButton(Challenge challenge, Island island, SkyblockPlayer player) {
        super(challenge.getIcon().getMaterial(), challenge.getIcon().getData(),
                ChatColor.DARK_PURPLE + challenge.getDisplayName());

        this.challenge = challenge;
        this.island = island;
        this.player = player;
    }

    @Override
    public ItemStack createItem() {
        setAllowRepaint(false);

        if (challenge.isLocked(player)) {
            TextBuilder lore = TextBuilder.create("Requires: ").gray();
            boolean first = true;
            for (Challenge c : challenge.getRequiredChallenges()) {
                if (!first) {
                    lore.append(", ").gray();
                } else {
                    first = false;
                }
                lore.append(c.getDisplayName()).white();
            }

            setIcon(Material.STAINED_GLASS_PANE, (byte) 7);
            setTitle(ChatColor.DARK_GRAY + "Locked");
            setDescription(lore.getLines());
        } else {
            if (island != null) {
                int finishedCount = player.getChallengeFinishedCount(challenge);
                if (finishedCount == 0) {
                    switch (challenge.getDifficulty()) {
                        case EASY:
                            setIcon(Material.STAINED_GLASS_PANE, (byte) 12);
                            break;
                        case MEDIUM:
                            setIcon(Material.STAINED_GLASS_PANE, (byte) 0);
                            break;
                        case HARD:
                            setIcon(Material.STAINED_GLASS_PANE, (byte) 4);
                            break;
                        case EXPERT:
                            setIcon(Material.STAINED_GLASS_PANE, (byte) 3);
                            break;
                    }
                }

                if (challenge.isRepeatable() || finishedCount == 0) {
                    TextBuilder description = TextBuilder.create()
                            .append(challenge.getDescription()).gray().newLine();
                    if (challenge.isTakingItems()) {
                        description.append("This challenge will take the required items!").red().italic().newLine();
                    }
                    if (challenge.isRemovingEntities()) {
                        description.append("This challenge will remove the required mobs!").red().italic().newLine();
                    }
                    setDescription(description
                            .append("Rewards: ").white().append(challenge.getRewardsText(player)).gray().newLine()
                            .append("Click to finish!").yellow().getLines());
                    setOnClick(new ClickListener() {
                        @Override
                        public void clicked(InventoryClickEvent event) {
                            int finished = 0;

                            while (challenge.finish(player, island)) {
                                player.onChallengeFinished(challenge);
                                finished++;

                                if (!event.isShiftClick()) {
                                    break;
                                }
                            }

                            if (finished > 0) {
                                setIcon(challenge.getIcon().getMaterial(), challenge.getIcon().getData());
                                getParentView().repaint();

                                if (finished == 1) {
                                    TextBuilder.create("You just finished ").green()
                                            .append(challenge.getDisplayName()).yellow()
                                            .append(". Congratulations!").green()
                                            .sendTo(player.getBukkitPlayer());
                                } else if (finished == 2) {
                                    TextBuilder.create("You just finished ").green()
                                            .append(challenge.getDisplayName()).yellow()
                                            .append(", twice. Congratulations!").green()
                                            .sendTo(player.getBukkitPlayer());
                                } else {
                                    TextBuilder.create("You just finished ").green()
                                            .append(challenge.getDisplayName()).yellow()
                                            .append(", " + finished + " times. Congratulations!").green()
                                            .sendTo(player.getBukkitPlayer());
                                }
                            } else {
                                TextBuilder.create("You don't have everything you need to finish ").red()
                                        .append(challenge.getDisplayName()).yellow().append(". ").red()
                                        .append(challenge.getDescription()).red()
                                        .sendTo(player.getBukkitPlayer());
                            }
                        }
                    });
                } else {
                    setDescription(TextBuilder.create(challenge.getDescription()).gray().getLines());
                }
            } else {
                setDescription(TextBuilder.create(challenge.getDescription()).gray().getLines());
            }
        }

        setAllowRepaint(true);
        return super.createItem();
    }
}
