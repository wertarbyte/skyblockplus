package de.craften.plugins.skyblockplus.gui;

import com.google.common.collect.ArrayListMultimap;
import de.craften.plugins.mcguilib.Button;
import de.craften.plugins.mcguilib.ClickListener;
import de.craften.plugins.mcguilib.MultiPageView;
import de.craften.plugins.skyblockplus.challenges.Challenge;
import de.craften.plugins.skyblockplus.challenges.ChallengeDifficulty;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * A multi-page view for challenges.
 */
class ChallengeView extends MultiPageView {
    private ArrayListMultimap<ChallengeDifficulty, Challenge> challenges;
    private final SkyblockPlayer player;
    private final Island island;

    /**
     * @param player player that will use this view
     * @param island island to finish challenges on when buttons are clicked, may be null
     */
    public ChallengeView(SkyblockPlayer player, Island island) {
        super(ChatColor.DARK_BLUE + "Challenges", 4 * 9); //one row for navigation is added by MultiPageView
        this.player = player;
        this.island = island;

        this.challenges = ArrayListMultimap.create();
        Challenge[] sortedChallenges = island.getWorld().getChallenges().toArray(new Challenge[island.getWorld().getChallenges().size()]);
        Arrays.sort(sortedChallenges, new ChallangeOrderComparator());

        for (Challenge challenge : sortedChallenges) {
            this.challenges.put(challenge.getDifficulty(), challenge);
        }
    }

    @Override
    protected void onCreateInventory() {
        removeAllButtons();

        insertElement(0, new Button(Material.DIRT, "Easy"));
        insertElement(9, new Button(Material.IRON_BLOCK, "Medium"));
        insertElement(18, new Button(Material.GOLD_BLOCK, "Hard"));
        insertElement(27, new Button(Material.DIAMOND_BLOCK, "Expert"));

        int start = getPage() * 8;
        int offset = 1;
        for (ChallengeDifficulty difficulty : new ChallengeDifficulty[]{ChallengeDifficulty.EASY,
                ChallengeDifficulty.MEDIUM, ChallengeDifficulty.HARD, ChallengeDifficulty.EXPERT}) {
            List<Challenge> challenges = this.challenges.get(difficulty);

            for (int i = 0; i < 8 && start + i < challenges.size(); i++) {
                insertElement(i + offset, new ChallengeButton(challenges.get(start + i), island, player));
            }

            offset += 9;
        }

        if (getPage() == 0) {
            Button backButton = new Button(Material.SIGN, "Back");
            backButton.setOnClick(new ClickListener() {
                public void clicked(InventoryClickEvent event) {
                    SkyblockGui.getInstance().showMenu(getViewer());
                }
            });
            insertElement(this.getSize(), backButton);
        }

        addNavigationButtons();
    }

    @Override
    public int getPageCount() {
        //calculate length of longest challenge list
        int max = Integer.MIN_VALUE;
        for (Collection<Challenge> list : challenges.asMap().values()) {
            if (list.size() > max) {
                max = list.size();
            }
        }

        //8 challenges per page, per list, so...
        return (int) Math.ceil(max / 8d);
    }
}

