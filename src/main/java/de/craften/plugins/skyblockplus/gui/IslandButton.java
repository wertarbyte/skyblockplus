package de.craften.plugins.skyblockplus.gui;

import de.craften.plugins.mcguilib.Button;
import de.craften.plugins.mcguilib.ClickListener;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * A button for an island.
 */
class IslandButton extends Button {
    public IslandButton(final Island island, final SkyblockPlayer player) {
        super(Material.COMPASS, "Island " + island.getId());
        if (island.getLeader().getUniqueId().equals(player.getUniqueId())) {
            if (island.getName() != null) {
                setDescription(
                        ChatColor.GRAY + "\"" + island.getName() + "\"",
                        ChatColor.GRAY + "Click to teleport");
            } else {
                setDescription(ChatColor.GRAY + "Click to teleport");
            }
        } else {
            if (island.getName() != null) {
                setDescription(
                        ChatColor.GRAY + "\"" + island.getName() + "\"",
                        ChatColor.GRAY + "by " + island.getLeader().getName(),
                        ChatColor.GRAY + "Click to teleport");
            } else {
                setDescription(
                        ChatColor.GRAY + "by " + island.getLeader().getName(),
                        ChatColor.GRAY + "Click to teleport");
            }
        }
        setOnClick(new ClickListener() {
            @Override
            public void clicked(InventoryClickEvent inventoryClickEvent) {
                inventoryClickEvent.getWhoClicked().teleport(island.getHomeLocation());
                getParentView().close();
            }
        });
    }
}
