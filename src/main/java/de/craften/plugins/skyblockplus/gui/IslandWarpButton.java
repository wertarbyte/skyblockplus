package de.craften.plugins.skyblockplus.gui;

import de.craften.plugins.mcguilib.Button;
import de.craften.plugins.mcguilib.ClickListener;
import de.craften.plugins.skyblockplus.skyblock.Island;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * A button for an island.
 */
class IslandWarpButton extends Button {
    public IslandWarpButton(final Island island, final OfflinePlayer warpToPlayer) {
        super(Material.COMPASS, "Island " + island.getId());
        if (island.getName() != null) {
            setDescription(
                    ChatColor.GRAY + "\"" + island.getName() + "\"",
                    ChatColor.GRAY + "Click to warp");
        } else {
            setDescription(
                    ChatColor.GRAY + "Click to warp");
        }
        if (island.getLeader().getUniqueId().equals(warpToPlayer.getUniqueId())) {
            if (island.getName() != null) {
                setDescription(
                        ChatColor.GRAY + "\"" + island.getName() + "\"",
                        ChatColor.GRAY + "Click to warp");
            } else {
                setDescription(ChatColor.GRAY + "Click to teleport");
            }
        } else {
            if (island.getName() != null) {
                setDescription(
                        ChatColor.GRAY + "\"" + island.getName() + "\"",
                        ChatColor.GRAY + "by " + island.getLeader().getName(),
                        ChatColor.GRAY + "Click to warp");
            } else {
                setDescription(
                        ChatColor.GRAY + "by " + island.getLeader().getName(),
                        ChatColor.GRAY + "Click to warp");
            }
        }
        setOnClick(new ClickListener() {
            @Override
            public void clicked(InventoryClickEvent inventoryClickEvent) {
                inventoryClickEvent.getWhoClicked().teleport(island.getWarpLocation());
                getParentView().close();
            }
        });
    }
}
