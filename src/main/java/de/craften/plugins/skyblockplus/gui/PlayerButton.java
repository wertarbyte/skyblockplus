package de.craften.plugins.skyblockplus.gui;

import de.craften.plugins.mcguilib.Button;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

/**
 * A button that shows a player's head.
 */
class PlayerButton extends Button {
    private String nickname;

    /**
     * Creates a new player head button.
     *
     * @param nickname    nickname of the player, if it is null, Steve's head is shown
     * @param title       title
     * @param description description
     */
    public PlayerButton(String nickname, String title, String... description) {
        super(Material.AIR, title, description);
        this.nickname = nickname;
    }

    /**
     * Gets the nickname of the head.
     *
     * @return the nickname of the head
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Sets the nickname of the head.
     *
     * @param nickname the new nickname
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
        repaint();
    }

    @Override
    public ItemStack createItem() {
        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta meta = (SkullMeta) head.getItemMeta();

        if (nickname != null) {
            meta.setOwner(nickname);
        }
        meta.setDisplayName(getTitle());
        meta.setLore(getDescription());

        head.setItemMeta(meta);
        return head;
    }
}
