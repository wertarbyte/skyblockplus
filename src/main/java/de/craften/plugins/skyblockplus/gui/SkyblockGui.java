package de.craften.plugins.skyblockplus.gui;

import de.craften.plugins.mcguilib.*;
import de.craften.plugins.mcguilib.text.TextBuilder;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import de.craften.plugins.skyblockplus.util.Item;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.List;
import java.util.logging.Level;

/**
 * A inventory-bases skyblock GUI.
 */
public class SkyblockGui {
    private final SkyblockPlus plugin;
    private final ViewManager viewManager;
    private static SkyblockGui instance;

    public SkyblockGui(SkyblockPlus plugin) {
        instance = this;
        this.plugin = plugin;
        this.viewManager = new ViewManager(plugin);
    }

    static SkyblockGui getInstance() {
        return instance;
    }

    public void showMenu(final Player player) {
        SinglePageView menu = new SinglePageView(ChatColor.DARK_BLUE + "SkyblockPlus Menu", 18);

        final List<Island> islands = plugin.getPlayer(player).getIslands();
        if (islands.isEmpty()) {
            final Button createButton = new Button(Material.ENDER_PEARL, "Start an island");
            createButton.setOnClick(new ClickListener() {
                @Override
                public void clicked(InventoryClickEvent inventoryClickEvent) {
                    if (player.hasPermission("skyblock.island.create")) {
                        try {
                            Plot plot = PlotPlus.getApi().getWorld(player.getWorld()).claimAutomatically(player);
                            plot.getWorld().clearPlot(plot); //ensures that the island is cleared
                            plot.teleportPlayer(player);
                            createButton.getParentView().close();
                        } catch (PersistenceException e) {
                            plugin.getLogger().log(Level.SEVERE, "Could not claim a new island plot", e);
                            player.sendMessage("An error occurred while creating your island. Try again!");
                        }
                    } else {
                        player.sendMessage("You have no permission to create new islands.");
                    }
                }
            });
            menu.insertElement(0, createButton);
        } else {
            if (islands.size() == 1) {
                menu.insertElement(0, new IslandButton(islands.get(0), plugin.getPlayer(player)));
            } else {
                Button teleportButton = new Button(Material.COMPASS, "Show islands");
                teleportButton.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent inventoryClickEvent) {
                        showIslandsMenu(player);
                    }
                });
                menu.insertElement(0, teleportButton);
            }
        }

        final Island island = plugin.getIslandAt(player);

        if (island != null) {
            if (island.mayBuild(player) || player.hasPermission("skyblock.island.manageany")) {
                Button challengeButton = new Button(Material.DIAMOND_ORE, "Challenges");
                challengeButton.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent inventoryClickEvent) {
                        showChallengeMenu(player, island);
                    }
                });
                menu.insertElement(1, challengeButton);

                final Button calculateLevelButton = new Button(Material.EXP_BOTTLE, "Island level: " + island.getLevel(),
                        ChatColor.GRAY + "Click to recalculate");

                calculateLevelButton.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent inventoryClickEvent) {
                        island.recalculateLevel();
                        calculateLevelButton.setTitle("Island level: " + island.getLevel());
                    }
                });
                menu.insertElement(2, calculateLevelButton);


                final Button showTeamButton = new PlayerButton(null, "Show your island team");
                showTeamButton.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent inventoryClickEvent) {
                        showTeamMenu(player, island);
                    }
                });
                menu.insertElement(4, showTeamButton);

                Item biomeIcon = BiomeView.getIcon(island.getBiome());
                final Button setBiomeButton = new Button(biomeIcon.getMaterial(), biomeIcon.getData(),
                        "Biome: " + island.getBiome().name(), ChatColor.GRAY + "Click to change");
                setBiomeButton.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent inventoryClickEvent) {
                        showBiomeMenu(player, island);
                    }
                });
                menu.insertElement(5, setBiomeButton);

                final Checkbox configWarpButton = new Checkbox(Material.WOOL, (byte) 15, Material.WOOL, (byte) 0,
                        "Island Warp",
                        ChatColor.GRAY + "Status: " + (island.isWarpEnabled() ? "Enabled" : "Disabled"));
                configWarpButton.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent inventoryClickEvent) {
                        island.setWarpEnabled(configWarpButton.isChecked());
                        configWarpButton.setDescription(ChatColor.GRAY + "Status: " +
                                (island.isWarpEnabled() ? "Enabled" : "Disabled"));
                    }
                });
                configWarpButton.setChecked(island.isWarpEnabled());
                menu.insertElement(6, configWarpButton);

                final Button setWarpButton = new Button(Material.HOPPER, "Set warp location");
                setWarpButton.setDescription(TextBuilder.create()
                        .append("Click to set the warp location of this island to your current location").gray()
                        .getLines());
                setWarpButton.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent inventoryClickEvent) {
                        island.setWarpLocation(player.getLocation());
                        player.sendMessage("Warp location updated.");
                    }
                });
                menu.insertElement(15, setWarpButton);


                final Button setHomeButton = new Button(Material.BED, "Set teleport location");
                setHomeButton.setDescription(TextBuilder.create()
                        .append("Click to set the teleport location of this island to your current location").gray()
                        .getLines());

                setHomeButton.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent inventoryClickEvent) {
                        island.setHomeLocation(player.getLocation());
                        player.sendMessage("Teleport location updated.");
                    }
                });
                menu.insertElement(9, setHomeButton);
            }
        }

        viewManager.showView(player, menu);
    }

    private void showTeamMenu(final Player player, final Island island) {
        viewManager.showView(player, new TeamView(player, island));
    }

    public void showChallengeMenu(final Player player, final Island island) {
        viewManager.showView(player, new ChallengeView(plugin.getPlayer(player), island));
    }

    public void showIslandsMenu(final Player player) {
        MultiPageView islandsView = new MultiPageView(ChatColor.DARK_BLUE + "Islands", 18);
        SkyblockPlayer skyblockPlayer = plugin.getPlayer(player);
        for (Island island : skyblockPlayer.getIslands()) {
            islandsView.addElement(new IslandButton(island, skyblockPlayer));
        }

        viewManager.showView(player, islandsView);
    }

    public void showBiomeMenu(final Player player, final Island island) {
        viewManager.showView(player, new BiomeView(island));
    }

    /**
     * Closes all views.
     */
    public void closeAll() {
        viewManager.closeAll();
    }

    /**
     * Shows a menu with warp buttons for the given islands.
     *
     * @param warpToPlayer name of the player these islands belong to
     * @param islands      islands to show
     * @param player       player to teleport to the islands
     */
    public void showWarpMenu(OfflinePlayer warpToPlayer, List<Island> islands, Player player) {
        MultiPageView islandsView = new MultiPageView(ChatColor.DARK_BLUE + "Islands of " + warpToPlayer.getName(), 18);
        for (Island island : islands) {
            islandsView.addElement(new IslandWarpButton(island, warpToPlayer));
        }

        viewManager.showView(player, islandsView);
    }
}
