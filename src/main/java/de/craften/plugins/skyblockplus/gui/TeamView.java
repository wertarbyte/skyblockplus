package de.craften.plugins.skyblockplus.gui;

import de.craften.plugins.mcguilib.Button;
import de.craften.plugins.mcguilib.ClickListener;
import de.craften.plugins.mcguilib.MultiPageView;
import de.craften.plugins.mcguilib.text.TextBuilder;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.io.IOException;

/**
 * A view for island teams.
 */
class TeamView extends MultiPageView {
    private Player player;
    private Island island;

    public TeamView(final Player player, final Island island) {
        super(ChatColor.DARK_BLUE + "Island Team", 18);
        this.player = player;
        this.island = island;

        SkyblockPlayer leader = island.getLeader();
        addElement(new PlayerButton(leader.getName(), leader.getName(), ChatColor.GRAY + "Leader"));

        for (final SkyblockPlayer helper : island.getTeam()) {
            final Button button = new PlayerButton(helper.getName(), helper.getName());
            if (island.mayManage(player.getUniqueId())) {
                button.setDescription(ChatColor.GRAY + "Click to remove " + helper.getName() + " from the team");
                button.setOnClick(new ClickListener() {
                    @Override
                    public void clicked(InventoryClickEvent inventoryClickEvent) {
                        try {
                            island.removeTeamMember(helper);
                            button.remove();
                            player.sendMessage(helper.getName() + " was removed from the team.");
                        } catch (IOException e) {
                            player.sendMessage(helper.getName() + " could not be removed from the team.");
                        }
                    }
                });
            }
            addElement(button);
        }
    }

    @Override
    protected void onCreateInventory() {
        super.onCreateInventory();

        if (island.mayManage(player)) {
            insertElement(this.getSize() + 4, new Button(Material.BOOK_AND_QUILL,
                    ChatColor.DARK_PURPLE + "Hint:",
                    ChatColor.GRAY + "Use " + ChatColor.WHITE + "/is team add <playername>",
                    ChatColor.GRAY + "to add players."));
        }

        if (island.mayBuild(player) && !island.getLeader().getUniqueId().equals(player.getUniqueId())) {
            //player is island member but not the leader
            Button leaveButton = new Button(Material.WOOD_DOOR,
                    ChatColor.RED + "Leave this island",
                    ChatColor.GRAY + "Click to leave this island.");
            leaveButton.setDescription(TextBuilder.create("Click to leave this island. ").gray()
                    .append("This cannot be undone!").red().getLines());
            leaveButton.setOnClick(new ClickListener() {
                @Override
                public void clicked(InventoryClickEvent inventoryClickEvent) {
                    try {
                        island.removeTeamMember(SkyblockPlus.getPlugin(SkyblockPlus.class).getPlayer(player));
                        close();
                    } catch (IOException e) {
                        player.sendMessage("An error occurred while removing you from this island. Try again!");
                        e.printStackTrace();
                    }
                }
            });
            insertElement(this.getSize() + 5, leaveButton);
        }

        if (getPage() == 0) {
            Button backButton = new Button(Material.SIGN, "Back");
            backButton.setOnClick(new ClickListener() {
                public void clicked(InventoryClickEvent event) {
                    SkyblockGui.getInstance().showMenu(getViewer());
                }
            });
            insertElement(this.getSize(), backButton);
        }
    }
}
