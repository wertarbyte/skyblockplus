package de.craften.plugins.skyblockplus.listeners;

import de.craften.plugins.skyblockplus.SkyblockPlus;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

/**
 * Listener for mob events.
 */
public class MobListener implements Listener {
    private final SkyblockPlus plugin;

    public MobListener(SkyblockPlus plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        if (plugin.getWorld(event.getLocation().getWorld()) != null) {
            switch (event.getEntityType()) {
                case WITHER:
                case GHAST:
                case BLAZE:
                case ENDER_DRAGON:
                    event.setCancelled(true);
                    break;
            }
        }
    }
}
