package de.craften.plugins.skyblockplus.listeners;

import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockWorld;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Handles respawning players. They will respawn on the last visited island they may build on.
 */
public class RespawnListener implements Listener {
    private final SkyblockPlus plugin;
    private final Map<UUID, Island> lastIslands = new HashMap<>();

    public RespawnListener(SkyblockPlus plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Location location = event.getRespawnLocation();
        SkyblockWorld world = plugin.getWorld(location.getWorld());
        if (world != null) {
            Island lastIsland = lastIslands.get(event.getPlayer().getUniqueId());
            if (lastIsland != null) {
                event.setRespawnLocation(lastIsland.getHomeLocation());
            } else {
                event.setRespawnLocation(location.getWorld().getSpawnLocation());
            }
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        Player p = event.getPlayer();
        Island island = plugin.getIslandAt(p);
        if (island != null && island.mayBuild(p)) {
            lastIslands.put(p.getUniqueId(), island);
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        Island island = plugin.getIslandAt(p);
        if (island != null && island.mayBuild(p)) {
            lastIslands.put(p.getUniqueId(), island);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        lastIslands.remove(event.getPlayer().getUniqueId());
    }
}
