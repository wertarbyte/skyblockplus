package de.craften.plugins.skyblockplus.persistence;

import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;

import java.util.*;

/**
 * A caching wrapper for the persistence.
 */
public class CacheWrapper implements WritingSkyblockPersistence {
    private final WritingSkyblockPersistence persistence;
    private final FixedSizeMap<UUID, SkyblockPlayer> players;
    private final FixedSizeMap<PlotId, Island> islands;

    /**
     * Creates a new CacheWrapper.
     *
     * @param persistence persistence to wrap
     */
    private CacheWrapper(WritingSkyblockPersistence persistence) {
        this.persistence = persistence;
        players = new FixedSizeMap<>(50);
        islands = new FixedSizeMap<>(50);
    }

    /**
     * Wraps the given persistence.
     *
     * @param persistence persistence to wrap
     * @return caching wrapper for the given persistence
     */
    public static WritingSkyblockPersistence wrap(WritingSkyblockPersistence persistence) {
        return new CacheWrapper(persistence);
    }

    @Override
    public void savePlayer(SkyblockPlayer player) throws PersistenceException {
        persistence.savePlayer(player);
    }

    @Override
    public void saveIsland(Island island) throws PersistenceException {
        persistence.saveIsland(island);
    }

    @Override
    public SkyblockPlayer getPlayer(UUID uuid) throws PersistenceException {
        SkyblockPlayer player = players.get(uuid);
        if (player == null) {
            player = persistence.getPlayer(uuid);
            players.put(uuid, player);
        }
        return player;
    }

    @Override
    public Island getIsland(PlotId id) throws PersistenceException {
        Island island = islands.get(id);
        if (island == null) {
            island = persistence.getIsland(id);
            islands.put(id, island);
        }
        return island;
    }

    @Override
    public List<Island> getAllIslands(String world) throws PersistenceException {
        //not simply using persistence.getAllIslands allows us to use cached islands here
        ArrayList<Island> islands = new ArrayList<>();
        for (Plot plot : PlotPlus.getApi().getWorld(world).getPlots()) {
            islands.add(getIsland(plot.getId()));
        }
        islands.trimToSize();
        return islands;
    }

    @Override
    public void removeIsland(Island island) throws PersistenceException {
        persistence.removeIsland(island);
        islands.remove(island.getId());
    }

    /**
     * A map with a fixed maximum size.
     *
     * @param <K> key type
     * @param <V> value type
     */
    private static class FixedSizeMap<K, V> extends LinkedHashMap<K, V> {
        private int maxSize;

        public FixedSizeMap(int maxSize) {
            this.maxSize = maxSize;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
            return size() > maxSize;
        }
    }
}
