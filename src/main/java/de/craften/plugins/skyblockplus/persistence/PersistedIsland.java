package de.craften.plugins.skyblockplus.persistence;

import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockWorld;
import org.bukkit.Location;
import org.bukkit.block.Biome;

import java.util.logging.Level;

/**
 * A persisted {@link de.craften.plugins.skyblockplus.skyblock.Island}.
 */
class PersistedIsland extends Island {
    private WritingSkyblockPersistence persistence;

    public PersistedIsland(SkyblockWorld world, Plot plot, Location homeLocation,
                           Location warpLocation, boolean isWarpEnabled,
                           long biomeChangedTime, int level, WritingSkyblockPersistence persistence) {
        super(world, plot, homeLocation, warpLocation, isWarpEnabled, biomeChangedTime, level);
        this.persistence = persistence;
    }

    @Override
    public void setHomeLocation(Location location) {
        super.setHomeLocation(location);
        save();
    }

    @Override
    public void setWarpLocation(Location warpLocation) {
        super.setWarpLocation(warpLocation);
        save();
    }

    @Override
    public void setWarpEnabled(boolean isWarpEnabled) {
        super.setWarpEnabled(isWarpEnabled);
        save();
    }

    @Override
    public void setBiome(Biome biome) {
        super.setBiome(biome);
        save();
    }

    @Override
    public void recalculateLevel() {
        super.recalculateLevel();
        save();
    }

    private void save() {
        try {
            persistence.saveIsland(this);
        } catch (PersistenceException e) {
            SkyblockPlus.getPlugin(SkyblockPlus.class).getLogger().log(Level.SEVERE, "Could not save island", e);
        }
    }
}
