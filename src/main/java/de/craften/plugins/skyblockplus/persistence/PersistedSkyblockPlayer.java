package de.craften.plugins.skyblockplus.persistence;

import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.challenges.Challenge;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;

import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

/**
 * A persisted {@link de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer}.
 */
class PersistedSkyblockPlayer extends SkyblockPlayer {
    private WritingSkyblockPersistence persistence;

    public PersistedSkyblockPlayer(UUID uuid, Map<String, Map<String, Integer>> challengeCount, WritingSkyblockPersistence persistence) {
        super(uuid, challengeCount);
        this.persistence = persistence;
    }

    @Override
    public void onChallengeFinished(Challenge challenge) {
        super.onChallengeFinished(challenge);
        save();
    }

    @Override
    public void resetChallenges(String world) {
        super.resetChallenges(world);
        save();
    }

    private void save() {
        try {
            persistence.savePlayer(this);
        } catch (PersistenceException e) {
            SkyblockPlus.getPlugin(SkyblockPlus.class).getLogger().log(Level.SEVERE, "Could not save player", e);
        }
    }
}
