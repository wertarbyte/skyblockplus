package de.craften.plugins.skyblockplus.persistence;

/**
 * An exception that is thrown when a persistence action fails.
 */
public class PersistenceException extends Exception {
    /**
     * Creates a new PersistenceException.
     *
     * @param msg message
     * @param e   exception that caused this exception
     */
    public PersistenceException(String msg, Throwable e) {
        super(msg, e);
    }

    /**
     * Creates a new PersistenceException.
     *
     * @param msg message
     */
    public PersistenceException(String msg) {
        super(msg);
    }
}
