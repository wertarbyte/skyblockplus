package de.craften.plugins.skyblockplus.persistence;

import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;

import java.util.List;
import java.util.UUID;

/**
 * A skyblock persistence that only supports reading.
 */
public interface SkyblockPersistence {
    /**
     * Gets the player with the given UUID.
     *
     * @param uuid UUID of the player
     * @return player with the given UUID, never null
     * @throws de.craften.plugins.skyblockplus.persistence.PersistenceException if getting the player fails
     */
    public SkyblockPlayer getPlayer(UUID uuid) throws PersistenceException;

    /**
     * Loads the island in the given world with the given ID.
     *
     * @param id ID of the island
     * @throws de.craften.plugins.skyblockplus.persistence.PersistenceException if loading the island fails
     */
    public Island getIsland(PlotId id) throws PersistenceException;

    /**
     * Loads all island in the given world.
     *
     * @param world world of the islands
     * @return all islands of the given world
     * @throws de.craften.plugins.skyblockplus.persistence.PersistenceException if loading the islands fails
     */
    public List<Island> getAllIslands(String world) throws PersistenceException;

    /**
     * Removes the given island.
     *
     * @param island island to remove
     */
    public void removeIsland(Island island) throws PersistenceException;
}
