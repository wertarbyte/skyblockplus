package de.craften.plugins.skyblockplus.persistence;

import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;

/**
 * A persistance for islands and skyblock players.
 */
interface WritingSkyblockPersistence extends SkyblockPersistence {
    /**
     * Saves the given player.
     *
     * @param player player to save
     * @throws de.craften.plugins.skyblockplus.persistence.PersistenceException if saving the player fails
     */
    public void savePlayer(SkyblockPlayer player) throws PersistenceException;

    /**
     * Saves the given island.
     *
     * @param island island to save
     * @throws de.craften.plugins.skyblockplus.persistence.PersistenceException if saving the island fails
     */
    public void saveIsland(Island island) throws PersistenceException;
}
