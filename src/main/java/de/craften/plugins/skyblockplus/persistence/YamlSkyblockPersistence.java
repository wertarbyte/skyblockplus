package de.craften.plugins.skyblockplus.persistence;

import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.skyblock.Island;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * A skyblock persistence that uses yaml files.
 */
public class YamlSkyblockPersistence implements WritingSkyblockPersistence {
    private final File playerDirectory;
    private final File islandDirectory;

    public YamlSkyblockPersistence(File playerDirectory, File islandDirectory) {
        this.playerDirectory = playerDirectory;
        this.islandDirectory = islandDirectory;
    }

    @Override
    public SkyblockPlayer getPlayer(UUID uuid) {
        Map<String, Map<String, Integer>> challenges = new HashMap<>();

        File playerFile = new File(playerDirectory, uuid.toString() + ".yml");
        if (playerFile.exists()) {
            ConfigurationSection config = YamlConfiguration.loadConfiguration(playerFile);
            ConfigurationSection challengesConfig = config.getConfigurationSection("challenges");
            if (challengesConfig != null) {
                for (String worldName : challengesConfig.getKeys(false)) {
                    Map<String, Integer> worldChallenges = new HashMap<>();
                    ConfigurationSection world = challengesConfig.getConfigurationSection(worldName);
                    if (world != null) {
                        for (String challenge : world.getKeys(false)) {
                            worldChallenges.put(challenge, config.getInt("challenges." + worldName + "." + challenge, 0));
                        }
                        challenges.put(worldName, worldChallenges);
                    }
                }
            }
        }

        return new PersistedSkyblockPlayer(uuid, challenges, this);
    }

    @Override
    public void savePlayer(SkyblockPlayer player) throws PersistenceException {
        File playerFile = new File(playerDirectory, player.getUniqueId().toString() + ".yml");
        YamlConfiguration config = new YamlConfiguration();

        for (Map.Entry<String, Map<String, Integer>> world : player.getChallengeFinishedCounts().entrySet()) {
            for (Map.Entry<String, Integer> challenge : world.getValue().entrySet()) {
                config.set("challenges." + world.getKey() + "." + challenge.getKey(), challenge.getValue());
            }
        }

        try {
            config.save(playerFile);
        } catch (IOException e) {
            throw new PersistenceException("Could not save player file", e);
        }
    }

    @Override
    public Island getIsland(PlotId id) throws PersistenceException {
        Plot plot = PlotPlus.getApi().getWorld(id.getWorld()).getPlot(id.getIdX(), id.getIdZ());
        if (plot == null) {
            return null;
        }

        File islandFile = new File(islandDirectory, id.getWorld() + "-" + id.getIdX() + "-" + id.getIdZ() + ".yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(islandFile);
        Location homeLocation = null;
        Location warpLocation = null;
        if (islandFile.exists()) {
            if (config.getString("home", null) != null) {
                homeLocation = new Location(plot.getWorld().getBukkitWorld(),
                        config.getDouble("home.x"), config.getDouble("home.y"), config.getDouble("home.z"),
                        (float) config.getDouble("home.yaw"), (float) config.getDouble("home.pitch"));
            }

            if (config.getString("warp", null) != null) {
                warpLocation = new Location(plot.getWorld().getBukkitWorld(),
                        config.getDouble("warp.x"), config.getDouble("warp.y"), config.getDouble("warp.z"),
                        (float) config.getDouble("warp.yaw"), (float) config.getDouble("warp.pitch"));
            }
        }

        return new PersistedIsland(SkyblockPlus.getWorld(id.getWorld()), plot, homeLocation,
                warpLocation, config.getBoolean("warpEnabled", false),
                config.getLong("biomeChangedTime", 0), config.getInt("level", 0), this);
    }

    @Override
    public List<Island> getAllIslands(final String world) throws PersistenceException {
        ArrayList<Island> islands = new ArrayList<>();
        for (Plot plot : PlotPlus.getApi().getWorld(world).getPlots()) {
            islands.add(getIsland(plot.getId()));
        }
        islands.trimToSize();
        return islands;
    }

    @Override
    public void removeIsland(Island island) throws PersistenceException {
        PlotId id = island.getId();
        File islandFile = new File(islandDirectory, id.getWorld() + "-" + id.getIdX() + "-" + id.getIdZ() + ".yml");
        if (islandFile.exists() && !islandFile.delete()) {
            throw new PersistenceException("Could not delete island file.");
        }
    }

    @Override
    public void saveIsland(Island island) throws PersistenceException {
        PlotId id = island.getId();
        File islandFile = new File(islandDirectory, island.getWorld().getName() + "-" + id.getIdX() + "-" + id.getIdZ() + ".yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(islandFile);

        config.set("home.x", island.getHomeLocation().getX());
        config.set("home.y", island.getHomeLocation().getY());
        config.set("home.z", island.getHomeLocation().getZ());
        config.set("home.pitch", island.getHomeLocation().getPitch());
        config.set("home.yaw", island.getHomeLocation().getYaw());

        config.set("warp.x", island.getWarpLocation().getX());
        config.set("warp.y", island.getWarpLocation().getY());
        config.set("warp.z", island.getWarpLocation().getZ());
        config.set("warp.pitch", island.getWarpLocation().getPitch());
        config.set("warp.yaw", island.getWarpLocation().getYaw());
        config.set("warpEnabled", island.isWarpEnabled());

        config.set("biomeChangedTime", island.getBiomeChangedTime());
        config.set("level", island.getLevel());

        try {
            config.save(islandFile);
        } catch (IOException e) {
            throw new PersistenceException("Could not save island file", e);
        }
    }
}

