package de.craften.plugins.skyblockplus.skyblock;

import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.plotplus.util.region.Region;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.SkyblockWorldScheme;
import de.craften.plugins.skyblockplus.addons.Addons;
import de.craften.plugins.skyblockplus.addons.LwcAddon;
import org.bukkit.Location;
import org.bukkit.block.Biome;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

/**
 * A skyblock island.
 */
public abstract class Island {
    /**
     * Delay, in milliseconds, between level recalculations.
     */
    private static final int LEVEL_CALCULATION_DELAY_MS = 10_000;

    private final String world;
    private final Plot plot;
    private long lastCalculationTime = 0;
    private int level = -1;
    private Location homeLocation;
    private Location warpLocation;
    private boolean isWarpEnabled;
    private long biomeChangedTime;

    /**
     * Creates a new island with the given plot.
     *
     * @param world            world of the island
     * @param plot             plot of the island
     * @param homeLocation     home location of the plot, may be null
     * @param warpLocation     warp location of the plot, may be null
     * @param isWarpEnabled    warp status of this plot
     * @param biomeChangedTime timestamp of the time when this island's biome was last changed, 0 if it was never changed
     */
    public Island(SkyblockWorld world, Plot plot, Location homeLocation, Location warpLocation,
                  boolean isWarpEnabled, long biomeChangedTime, int level) {
        this.world = world.getName();
        this.plot = plot;
        this.homeLocation = homeLocation;
        this.warpLocation = warpLocation;
        this.isWarpEnabled = isWarpEnabled;
        this.biomeChangedTime = biomeChangedTime;
        this.level = level;
    }

    /**
     * Gets the ID of this island.
     *
     * @return ID of this island
     */
    public PlotId getId() {
        return getPlot().getId();
    }

    /**
     * Gets this island's level.
     *
     * @return this island's level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Recalculates this island's level. As this is based on blocks on the island, this method should only be called
     * from the main thread.
     */
    public void recalculateLevel() {
        if (System.currentTimeMillis() - lastCalculationTime > LEVEL_CALCULATION_DELAY_MS) {
            level = getWorld().calculateLevel(this);
            lastCalculationTime = System.currentTimeMillis();
        }
    }

    /**
     * Gets the plot of this island.
     *
     * @return the plot of this island
     */
    private Plot getPlot() {
        return plot;
    }

    /**
     * Gets the home location of this island. If none is configured, this returns the default teleport location.
     *
     * @return the home location of this island
     */
    public Location getHomeLocation() {
        if (homeLocation == null) {
            PlotWorld world = getPlot().getWorld();
            return world.getScheme().getTeleportLocation(world, getPlot().getIdX(), getPlot().getIdZ());
        } else {
            return homeLocation;
        }
    }

    /**
     * Sets the home location of this island.
     *
     * @param location the new home location of this island
     */
    public void setHomeLocation(Location location) {
        homeLocation = location;
    }

    /**
     * Gets the warp location of this island.
     *
     * @return the warp location of this island
     */
    public Location getWarpLocation() {
        if (warpLocation == null) {
            return getHomeLocation();
        }
        return warpLocation;
    }

    /**
     * Sets the warp location of this island.
     *
     * @param warpLocation the new warp location of this island
     */
    public void setWarpLocation(Location warpLocation) {
        this.warpLocation = warpLocation;
    }

    /**
     * Checks if warping to this island is enabled.
     *
     * @return true if warping is enabled, false if not
     */
    public boolean isWarpEnabled() {
        return isWarpEnabled;
    }

    /**
     * Enables or disables warping to this island.
     *
     * @param isWarpEnabled true to enable warping, false to disable it
     */
    public void setWarpEnabled(boolean isWarpEnabled) {
        this.isWarpEnabled = isWarpEnabled;
    }

    /**
     * Gets the last time the biome of this island was changed (as timestamp).
     *
     * @return timestamp of the time when the biome of this island was last changed, 0 if it was never changed
     */
    public long getBiomeChangedTime() {
        return biomeChangedTime;
    }

    /**
     * Sets the biome of this island.
     *
     * @param biome new biome of this island
     */
    public void setBiome(Biome biome) {
        if (getPlot().getBiome() != biome) {
            getPlot().setBiome(biome);
            biomeChangedTime = System.currentTimeMillis();
        }
    }

    /**
     * Initializes this island by filling the chest.
     */
    public void initialize() {
        SkyblockWorldScheme structure = (SkyblockWorldScheme) plot.getWorld().getScheme();
        Chest chest = structure.getStartChest(plot.getWorld(), plot.getIdX(), plot.getIdZ());
        if (chest != null) {
            Inventory inventory = chest.getBlockInventory();
            inventory.clear(); //make sure the chest only contains the items we want it to contain
            getWorld().getStartItems().putInto(inventory);
            chest.update();

            if (Addons.isLwcAvailable()) {
                LwcAddon.addProtection(this, chest);
            }
        }
    }

    /**
     * Gets the world this island is located in.
     *
     * @return the world this island is located in
     */
    public SkyblockWorld getWorld() {
        return SkyblockPlus.getWorld(world);
    }

    /**
     * Gets the center of this island.
     *
     * @return the center of this island
     */
    public final PointXZ getCenter() {
        PlotWorld world = getPlot().getWorld();
        return ((SkyblockWorldScheme) world.getScheme()).getCenter(getPlot().getIdX(), getPlot().getIdZ());
    }

    /**
     * Gets the leader of this island.
     *
     * @return the leader of this island
     */
    public SkyblockPlayer getLeader() {
        return SkyblockPlus.getPlugin(SkyblockPlus.class).getPlayer(getPlot().getOwner().getUniqueId());
    }

    /**
     * Gets the team of this island.
     *
     * @return the team of this island
     */
    public Iterable<SkyblockPlayer> getTeam() {
        return new Iterable<SkyblockPlayer>() {
            @Override
            public Iterator<SkyblockPlayer> iterator() {
                return new Iterator<SkyblockPlayer>() {
                    private Iterator<SimplePlayer> helperIterator = getPlot().getHelpers().iterator();

                    public boolean hasNext() {
                        return helperIterator.hasNext();
                    }

                    @Override
                    public SkyblockPlayer next() {
                        return SkyblockPlus.getPlugin(SkyblockPlus.class).getPlayer(helperIterator.next().getUniqueId());
                    }

                    @Override
                    public void remove() {
                    }
                };
            }
        };
    }

    /**
     * Checks if the player with the given UUID may manage this island (i.e. remove, reset or add team members to it).
     *
     * @param uuid UUID of a player
     * @return true if the player with the given UUID may manage this island, false if not
     */
    public boolean mayManage(UUID uuid) {
        return getPlot().mayManage(uuid);
    }

    /**
     * Checks if the given player may manage this island (i.e. remove, reset or add team members to it).
     *
     * @param player player
     * @return true if the given player may manage this island, false if not
     */
    public boolean mayManage(Player player) {
        return getPlot().mayManage(player);
    }

    /**
     * Checks if the player with the given UUID may build on this island.
     *
     * @param uuid UUID of a player
     * @return true if the player with the given UUID may build on this island, false if not
     */
    public boolean mayBuild(UUID uuid) {
        return getPlot().mayBuild(uuid);
    }

    /**
     * Checks if the given player may build on this island.
     *
     * @param player player
     * @return true if the given player may build on this island, false if not
     */
    public boolean mayBuild(Player player) {
        return getPlot().mayBuild(player);
    }

    /**
     * Adds the given player to this island's team. If the player is already in this team, nothing is done.
     *
     * @param player player to add
     * @throws java.io.IOException if adding the player fails
     */
    public void addTeamMember(SkyblockPlayer player) throws IOException {
        if (!mayBuild(player.getUniqueId())) {
            try {
                getPlot().addHelper(new SimplePlayer(player.getUniqueId(), player.getName()));
            } catch (PersistenceException e) {
                throw new IOException(e);
            }
        }
    }

    /**
     * Removes the given player from this island's team. If this is the only island of that player, the challenges are
     * reset and the inventory of the player is cleared.
     * <p/>
     * If the player isn't in this team, nothing is done.
     *
     * @param player player to remove
     * @throws java.lang.IllegalStateException if the player to remove is the leader of this island
     * @throws java.io.IOException             if removing the player fails
     */
    public void removeTeamMember(SkyblockPlayer player) throws IOException {
        if (mayBuild(player.getUniqueId())) {
            if (getPlot().getOwner().getUniqueId().equals(player.getUniqueId())) {
                throw new IllegalStateException("The leader can't be removed from the island's team");
            }

            try {
                getPlot().removeHelper(player.getUniqueId());
            } catch (PersistenceException e) {
                throw new IOException(e);
            }

            if (getWorld().getIslandsOf(player).isEmpty()) {
                player.resetChallenges(getWorld().getName());

                Player onlinePlayer = player.getBukkitPlayer();
                if (onlinePlayer != null) {
                    onlinePlayer.getInventory().clear();
                }
                //TODO unfortunatly, we can't clear the inventory of offline players atm, but we should to that
            }
        }
    }

    /**
     * Restarts this island. If this is the only island of the leader or one of the team members, the inventories of
     * these players are cleared and their challenges are reset.
     */
    public void restart() {
        SkyblockPlayer leader = SkyblockPlus.getPlugin(SkyblockPlus.class).getPlayer(getPlot().getOwner().getUniqueId());
        Collection<SimplePlayer> team = getPlot().getHelpers();

        getPlot().getWorld().clearPlot(getPlot());
        setHomeLocation(null);
        setWarpLocation(null);
        setBiome(getPlot().getWorld().getConfig().getDefaultBiome());

        if (getWorld().getIslandsOf(leader).size() == 1) {
            leader.resetChallenges(getWorld().getName());
            Player onlinePlayer = leader.getBukkitPlayer();
            if (onlinePlayer != null) {
                onlinePlayer.getInventory().clear();
            }
        }

        for (SimplePlayer player : team) {
            SkyblockPlayer skyblockPlayer = SkyblockPlus.getPlugin(SkyblockPlus.class).getPlayer(player.getUniqueId());
            if (getWorld().getIslandsOf(skyblockPlayer).size() == 1) {
                skyblockPlayer.resetChallenges(getWorld().getName());
                Player onlinePlayer = leader.getBukkitPlayer();
                if (onlinePlayer != null) {
                    onlinePlayer.getInventory().clear();
                }
                //TODO unfortunatly, we can't clear the inventory of offline players atm, but we should to that
            }
        }
    }

    /**
     * Removes this island. If this is the only island of the leader or one of the team members, the inventories of
     * these players are cleared and their challenges are reset.
     *
     * @throws java.io.IOException if removing the island fails
     */
    public void remove() throws IOException {
        //Save leader and team now, they'll be gone after resetting the plot
        SkyblockPlayer leader = SkyblockPlus.getPlugin(SkyblockPlus.class).getPlayer(getPlot().getOwner().getUniqueId());
        Collection<SimplePlayer> team = getPlot().getHelpers();

        Chest startChest = ((SkyblockWorldScheme) getPlot().getWorld().getScheme()).getStartChest(plot.getWorld(), plot.getIdX(), plot.getIdZ());
        if (startChest != null && Addons.isLwcAvailable()) {
            LwcAddon.resetProtections(startChest);
        }

        try {
            getPlot().getWorld().resetPlot(getPlot());
        } catch (PersistenceException e) {
            throw new IOException(e);
        }

        if (getWorld().getIslandsOf(leader).isEmpty()) {
            leader.resetChallenges(getWorld().getName());
            Player onlinePlayer = leader.getBukkitPlayer();
            if (onlinePlayer != null) {
                onlinePlayer.getInventory().clear();
            }
        }

        for (SimplePlayer player : team) {
            SkyblockPlayer skyblockPlayer = SkyblockPlus.getPlugin(SkyblockPlus.class).getPlayer(player.getUniqueId());
            if (getWorld().getIslandsOf(skyblockPlayer).isEmpty()) {
                skyblockPlayer.resetChallenges(getWorld().getName());
                Player onlinePlayer = leader.getBukkitPlayer();
                if (onlinePlayer != null) {
                    onlinePlayer.getInventory().clear();
                }
                //TODO unfortunatly, we can't clear the inventory of offline players atm, but we should to that
            }
        }
    }

    /**
     * Gets the region of this island. This is only the inner region, not including connections to other islands.
     *
     * @return region of this island
     */
    public Region getRegion() {
        return getPlot().getCoreBuildRegion();
    }

    /**
     * Gets the name of this island.
     *
     * @return the name of this island or null if this island has no name
     */
    public String getName() {
        return getPlot().getConfig().getName();
    }

    /**
     * Sets the name of this island.
     *
     * @param name new name of this island or null to remove the name
     * @throws java.io.IOException if renaming the island fails
     */
    public void setName(String name) throws IOException {
        try {
            getPlot().getConfig().setName(name);
        } catch (PersistenceException e) {
            throw new IOException(e);
        }
    }

    /**
     * Gets the biome of this island.
     *
     * @return the biome of this island
     */
    public Biome getBiome() {
        return getPlot().getBiome();
    }

    @Override
    public String toString() {
        return "Island " + getPlot().getIdX() + ";" + getPlot().getIdZ() + " in " + getWorld().getName();
    }
}
