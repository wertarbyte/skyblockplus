package de.craften.plugins.skyblockplus.skyblock;

import de.craften.plugins.plotplus.util.PointXZ;
import de.craften.plugins.skyblockplus.util.MaterialUtil;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashMap;
import java.util.Map;

/**
 * A calculator for island levels.
 */
class IslandLevelCalculator {
    private int defaultRating = 10;
    private double pointsPerLevel;
    private Map<Material, Integer> blockValues;
    private Map<Material, Integer> blockLimits;
    private Map<Material, Integer> dynamicRating;

    public IslandLevelCalculator(ConfigurationSection config) {
        defaultRating = config.getInt("defaultRating", 10);
        pointsPerLevel = config.getDouble("pointsPerLevel", 1000);

        blockValues = new HashMap<>();
        ConfigurationSection blockRatingSection = config.getConfigurationSection("blockValues");
        for (String blockId : blockRatingSection.getKeys(false)) {
            blockValues.put(MaterialUtil.parse(blockId), blockRatingSection.getInt(blockId));
        }

        blockLimits = new HashMap<>();
        ConfigurationSection blockLimitsSection = config.getConfigurationSection("blockLimits");
        for (String blockId : blockLimitsSection.getKeys(false)) {
            blockLimits.put(MaterialUtil.parse(blockId), blockLimitsSection.getInt(blockId));
        }

        dynamicRating = new HashMap<>();
        ConfigurationSection dynamicRatingSection = config.getConfigurationSection("dynamicRating");
        for (String blockId : dynamicRatingSection.getKeys(false)) {
            dynamicRating.put(MaterialUtil.parse(blockId), dynamicRatingSection.getInt(blockId));
        }
    }

    /**
     * Calculates the level of the given island.
     *
     * @param island island to calculate the level of
     * @return level of the given island
     */
    public int calculateLevel(Island island) {
        return (int) (calculatePoints(island) / pointsPerLevel);
    }

    private int calculatePoints(Island island) {
        Map<Material, Integer> blocks = countBlocks(island);
        int points = 0;

        for (Map.Entry<Material, Integer> block : blocks.entrySet()) {
            int v = Math.min(getLimit(block.getKey()), block.getValue());

            Integer dynamic = dynamicRating.get(block.getKey());
            if (dynamic != null) {
                v = (int) Math.round(getDynamicRating(v, dynamic));
            }

            Integer rating = blockValues.get(block.getKey());
            v *= rating != null ? rating : defaultRating;

            points += v;
        }

        return points;
    }

    private int getLimit(Material material) {
        Integer limit = blockLimits.get(material);
        if (limit != null) {
            return limit;
        }
        return Integer.MAX_VALUE;
    }

    private double getDynamicRating(double value, double dynamic) {
        if (value < 0.0D) {
            return -getDynamicRating(-value, dynamic);
        }
        return (Math.sqrt(8.0 * value / dynamic + 1.0) - 1.0) / 2.0 * dynamic;
    }

    private Map<Material, Integer> countBlocks(Island island) {
        Map<Material, Integer> blocks = new HashMap<>();
        World world = island.getWorld().getBukkitWorld();
        for (PointXZ p : island.getRegion()) {
            for (int y = 0; y < world.getMaxHeight(); y++) {
                Block block = world.getBlockAt(p.getX(), y, p.getZ());
                if (block.getType() != Material.AIR) {
                    Integer blockRating = blocks.get(block.getType());
                    if (blockRating == null) {
                        blocks.put(block.getType(), 1);
                    } else {
                        blocks.put(block.getType(), blockRating + 1);
                    }
                }
            }
        }
        return blocks;
    }
}
