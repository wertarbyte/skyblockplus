package de.craften.plugins.skyblockplus.skyblock;

import com.google.common.collect.Lists;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.challenges.Challenge;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * A skyblock player.
 */
public abstract class SkyblockPlayer {
    private UUID uuid;
    private Map<String, Map<String, Integer>> challengeCount;

    public SkyblockPlayer(UUID uuid, Map<String, Map<String, Integer>> challengeCount) {
        this.uuid = uuid;
        this.challengeCount = challengeCount;
    }

    /**
     * Gets the name of this player.
     *
     * @return the name of this player
     */
    public String getName() {
        return Bukkit.getOfflinePlayer(uuid).getName();
    }

    /**
     * Gets the UUID of this player.
     *
     * @return the UUID of this player
     */
    public UUID getUniqueId() {
        return uuid;
    }

    /**
     * Gets the Bukkit player instance of this player.
     *
     * @return the Bukkit player instance of this player
     */
    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(uuid);
    }

    /**
     * Gets the number of times the given challenge was finished by this player.
     *
     * @param challenge challenge to check
     * @return number of times the given challenge was finished by this player
     */
    public int getChallengeFinishedCount(Challenge challenge) {
        Map<String, Integer> challenges = challengeCount.get(challenge.getWorld().getName());
        if (challenges != null) {
            Integer count = challenges.get(challenge.getName());
            return count != null ? count : 0;
        }
        return 0;
    }

    /**
     * Gets the number of times each challenge was finished.
     *
     * @return each challenge with a number of times it was finished
     */
    public Map<String, Integer> getChallengeFinishedCounts(String world) {
        Map<String, Integer> challenges = challengeCount.get(world);
        return challenges != null ? challenges : Collections.<String, Integer>emptyMap();
    }

    /**
     * Gets the number of times each challenge was finished.
     *
     * @return each challenge with a number of times it was finished
     */
    public Map<String, Map<String, Integer>> getChallengeFinishedCounts() {
        return challengeCount;
    }

    /**
     * Gets all islands of the given player, including islands the player may build on.
     *
     * @return a list of all islands of the given player
     */
    public List<Island> getIslands() {
        List<Island> islands = new ArrayList<>();
        for (SkyblockWorld world : SkyblockPlus.getWorlds()) {
            islands.addAll(world.getIslandsOf(this));
        }
        return islands;
    }

    /**
     * Gets all islands of the given player, including islands the player may build on, in the given world.
     *
     * @param world world
     * @return a list of all islands of the given player in the given world
     */
    public List<Island> getIslands(World world) {
        return Lists.newArrayList(SkyblockPlus.getWorld(world.getName()).getIslandsOf(this));
    }

    /**
     * Gets all islands of the player, including islands the player may build on, that other players are allowed
     * to warp to.
     *
     * @return a list of all islands of the given player where warp is enabled
     */
    public List<Island> getWarpableIslands() {
        List<Island> islands = new ArrayList<>();
        for (SkyblockWorld world : SkyblockPlus.getWorlds()) {
            for (Island island : world.getIslandsOf(this)) {
                if (island.isWarpEnabled()) {
                    islands.add(island);
                }
            }
        }
        return islands;
    }

    /**
     * Gets called when the player finished a challenge and increases the counter.
     *
     * @param challenge the finished challenge
     */
    public void onChallengeFinished(Challenge challenge) {
        Map<String, Integer> worldChallenges = challengeCount.get(challenge.getWorld().getName());
        if (worldChallenges == null) {
            worldChallenges = new HashMap<String, Integer>();
            worldChallenges.put(challenge.getName(), 1);
            challengeCount.put(challenge.getWorld().getName(), worldChallenges);
        } else {
            Integer count = worldChallenges.get(challenge.getName());
            if (count == null) {
                count = 1;
            } else {
                count++;
            }
            worldChallenges.put(challenge.getName(), count);
        }
    }

    /**
     * Resets all challenges of this player.
     *
     * @param world world to reset all challenges of this player in
     */
    public void resetChallenges(String world) {
        challengeCount.remove(world);
    }

    @Override
    public String toString() {
        return Bukkit.getOfflinePlayer(uuid).getName();
    }
}
