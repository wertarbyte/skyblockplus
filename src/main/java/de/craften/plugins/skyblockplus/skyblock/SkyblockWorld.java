package de.craften.plugins.skyblockplus.skyblock;

import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.challenges.Challenge;
import de.craften.plugins.skyblockplus.persistence.PersistenceException;
import de.craften.plugins.skyblockplus.util.ItemList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

import java.util.*;

/**
 * A skyblock world.
 */
public class SkyblockWorld {
    private final String name;
    private Map<String, Challenge> challenges;
    private final ItemList startItems;
    private final IslandLevelCalculator levelCalculator;
    private final Set<PlotId> ignoredIslands;

    public SkyblockWorld(String name, ConfigurationSection config, ConfigurationSection challenges, ConfigurationSection blockRatingConfig) {
        this.name = name;

        this.challenges = new HashMap<>();
        for (String challenge : challenges.getKeys(false)) {
            this.challenges.put(challenge, Challenge.fromConfig(challenge, challenges.getConfigurationSection(challenge), this));
        }

        levelCalculator = new IslandLevelCalculator(blockRatingConfig);
        this.startItems = new ItemList(config.getString("startItems"));

        this.ignoredIslands = new HashSet<>();
        for (String ignored : config.getStringList("ignoredIslands")) {
            String[] parts = ignored.split(";");
            ignoredIslands.add(new PlotId(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), name));
        }
    }

    /**
     * Gets the island with the given ID.
     *
     * @param plotId ID of the island to get
     * @return the island with the given ID or null if it doesn't exist or is an ignored island
     */
    public Island getIsland(PlotId plotId) {
        if (ignoredIslands.contains(plotId)) {
            return null;
        }

        try {
            return SkyblockPlus.getPlugin(SkyblockPlus.class).getPersistence().getIsland(plotId);
        } catch (PersistenceException e) {
            SkyblockPlus.getPlugin(SkyblockPlus.class).getLogger().severe("Could not load island " + plotId.toString());
            return null;
        }
    }

    /**
     * Gets the island at the given location.
     *
     * @param location location, must be in this world
     * @return island at the given location or null if there is now island at the given location
     */
    public Island getIslandAt(Location location) {
        Plot plot = PlotPlus.getApi().getWorld(getName()).getPlotAt(location);
        if (plot != null && !ignoredIslands.contains(plot.getId())) {
            return getIsland(plot.getId());
        }
        return null;
    }

    /**
     * Gets the name of this world.
     *
     * @return the name of this world
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the bukkit world instance of this world.
     *
     * @return bukkit world of this world
     */
    public World getBukkitWorld() {
        return Bukkit.getWorld(name);
    }

    /**
     * Gets the start items of a new island.
     *
     * @return the start items of a new island
     */
    public ItemList getStartItems() {
        return startItems;
    }

    /**
     * Gets all challenges.
     *
     * @return all challenges
     */
    public Collection<Challenge> getChallenges() {
        return challenges.values();
    }

    /**
     * Gets the challenge with the given name.
     *
     * @param name name of the challenge
     * @return challenge with the given name or null if a challenge with that name doesn't exist
     */
    public Challenge getChallenge(String name) {
        return challenges.get(name);
    }

    /**
     * Calculates the level of the given island.
     *
     * @return level of the given island
     */
    public int calculateLevel(Island island) {
        return levelCalculator.calculateLevel(island);
    }

    /**
     * Gets all islands of the given player in this world, including islands the player may build on.
     *
     * @param player player to get all islands of
     * @return list of all islands of the given player in this world
     */
    public Collection<Island> getIslandsOf(SkyblockPlayer player) {
        List<Island> islands = new ArrayList<>();
        for (Plot plot : PlotPlus.getApi().getWorld(getName()).getPlotsByOwner(player.getUniqueId())) {
            if (!ignoredIslands.contains(plot.getId())) {
                islands.add(getIsland(plot.getId()));
            }
        }
        for (Plot plot : PlotPlus.getApi().getWorld(getName()).getPlotsByHelper(player.getUniqueId())) {
            if (!ignoredIslands.contains(plot.getId())) {
                islands.add(getIsland(plot.getId()));
            }
        }
        return islands;
    }

    /**
     * Gets an iterator for all islands in this world.
     *
     * @return an iterator for all islands in this world
     */
    public Iterable<Island> getIslands() {
        return new Iterable<Island>() {
            @Override
            public Iterator<Island> iterator() {
                final Iterator<Plot> plots = PlotPlus.getApi().getWorld(getName()).getPlots().iterator();

                return new Iterator<Island>() {
                    private Island next = first();

                    @Override
                    public boolean hasNext() {
                        return next != null;
                    }

                    @Override
                    public Island next() {
                        if (next == null) {
                            throw new NoSuchElementException();
                        }

                        Island current = next;

                        //seach next not-ignored island
                        next = null;
                        while (plots.hasNext()) {
                            Plot nextPlot = plots.next();
                            if (!ignoredIslands.contains(nextPlot.getId())) {
                                next = getIsland(nextPlot.getId());
                                break;
                            }
                        }

                        return current;
                    }

                    private Island first() {
                        //seach next not-ignored island
                        while (plots.hasNext()) {
                            Plot first = plots.next();
                            if (!ignoredIslands.contains(first.getId())) {
                                return getIsland(first.getId());
                            }
                        }

                        return null;
                    }

                    @Override
                    public void remove() {
                    }
                };
            }
        };
    }
}
