package de.craften.plugins.skyblockplus.util;

/**
 * An item whose data value is ignored by {@link de.craften.plugins.skyblockplus.util.ItemList}.
 */
public class IgnoreDataItem extends Item {
    public IgnoreDataItem(int id) {
        super(id, 0);
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof IgnoreDataItem) && ((IgnoreDataItem) obj).getId() == getId();
    }

    @Override
    public int hashCode() {
        return getId() * 13;
    }

    @Override
    public String toString() {
        return "" + getId();
    }
}
