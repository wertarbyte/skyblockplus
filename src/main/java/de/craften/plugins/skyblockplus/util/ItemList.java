package de.craften.plugins.skyblockplus.util;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A checklist that counts items.
 */
public class ItemList {
    private Map<Item, Integer> items = new HashMap<>();

    /**
     * Creates a new empty item list.
     */
    public ItemList() {
    }

    /**
     * Creates a new checklist.
     *
     * @param itemString required items, syntax is "id[:data[:amount]] id[:data[:amount]] ..."
     * @throws java.lang.IllegalArgumentException if the given string is invalid
     */
    public ItemList(String itemString) {
        String[] items = itemString.split("\\s+");
        if (!items[0].isEmpty()) {
            for (String item : items) {
                String[] parts = item.split(":");
                if (parts.length == 1) {
                    add(new IgnoreDataItem(Integer.parseInt(parts[0])), 1);
                } else if (parts.length == 2) {
                    add(new Item(item), 1);
                } else if (parts.length == 3) {
                    if (parts[1].equals("*")) {
                        add(new IgnoreDataItem(Integer.parseInt(parts[0])), Integer.parseInt(parts[2]));
                    } else {
                        add(new Item(Integer.parseInt(parts[0]), Integer.parseInt(parts[1])), Integer.parseInt(parts[2]));
                    }
                } else {
                    throw new IllegalArgumentException("Invalid item syntax: " + item);
                }
            }
        }
    }

    /**
     * Creates a copy of the given checklist.
     *
     * @param list checklist to copy
     */
    public ItemList(ItemList list) {
        items = new HashMap<>(list.items);
    }

    /**
     * Adds the given amount of the given item.
     *
     * @param item  required item
     * @param count required amount if that item
     */

    public void add(Item item, int count) {
        Integer current = items.get(item);
        if (current != null) {
            items.put(item, current + count);
        } else {
            items.put(item, count);
        }
    }

    /**
     * Removes the given amount of the given item.
     *
     * @param item   item to remove
     * @param amount amount to remove of that item
     */
    public void remove(Item item, Integer amount) {
        Integer current = items.get(item);
        if (current != null) {
            if (current <= amount) {
                items.remove(item);
            } else {
                items.put(item, current - amount);
            }
        } else {
            item = new IgnoreDataItem(item.getId());
            current = items.get(item);
            if (current != null) {
                if (current <= amount) {
                    items.remove(item);
                } else {
                    items.put(item, current - amount);
                }
            }
        }
    }

    /**
     * Checks if this list is empty.
     *
     * @return true if this list is empty, false if not
     */
    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * Gets the items.
     *
     * @return the items
     */
    public Set<Map.Entry<Item, Integer>> getItems() {
        return items.entrySet();
    }

    /**
     * Gives all items in this list to the given player.
     *
     * @param player player to give all items to
     */
    public void giveTo(Player player) {
        putInto(player.getInventory());
    }

    /**
     * Puts all items into the given inventory.
     *
     * @param inventory inventory to put all items into
     */
    public void putInto(Inventory inventory) {
        for (Map.Entry<Item, Integer> item : getItems()) {
            inventory.addItem(item.getKey().getItemStack(item.getValue()));
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Item, Integer> k : items.entrySet()) {
            sb.append(k.getKey().toString()).append(":").append(k.getValue()).append(" ");
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }
}
