package de.craften.plugins.skyblockplus.util;

import org.bukkit.Material;

/**
 * Utility class for {@link org.bukkit.Material}.
 */
public class MaterialUtil {
    private MaterialUtil() {
    }

    /**
     * Gets the {@link org.bukkit.Material} for the given string.
     *
     * @param string case-insensitive string representation of a material, either an ID or a name
     * @return parsed material
     */
    public static Material parse(String string) {
        Material m;
        if (string.matches("\\d+")) {
            m = Material.getMaterial(Integer.parseInt(string));
        } else {
            m = Material.getMaterial(string.toUpperCase());
        }

        if (m == null) {
            throw new IllegalArgumentException("Unknown material: " + string);
        }
        return m;
    }
}
