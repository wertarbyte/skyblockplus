package de.craften.plugins.skyblockplus.util;

/**
 * String utils.
 */
public class StringUtil {
    private StringUtil() {
    }

    /**
     * Joins all elements in the given array, starting at startIndex, using the given delimiter.
     *
     * @param array      array of strings to join
     * @param delimiter  delimiter
     * @param startIndex index of the first string to join
     * @return joined string
     */
    public static String join(String[] array, String delimiter, int startIndex) {
        StringBuilder sb = new StringBuilder();
        for (int i = startIndex; i < array.length; i++) {
            sb.append(array[i]).append(delimiter);
        }
        sb.setLength(sb.length() - delimiter.length());
        return sb.toString();
    }

    /**
     * Joins all elements in the given array using the given delimiter.
     *
     * @param array     array of strings to join
     * @param delimiter delimiter
     * @return joined string
     */
    public static String join(String[] array, String delimiter) {
        return join(array, delimiter, 0);
    }
}
