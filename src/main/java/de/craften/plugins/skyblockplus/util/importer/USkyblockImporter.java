package de.craften.plugins.skyblockplus.util.importer;

import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.SimplePlayer;
import de.craften.plugins.plotplus.persistence.PersistenceException;
import de.craften.plugins.plotplus.persistence.entities.PlotId;
import de.craften.plugins.plotplus.plot.Plot;
import de.craften.plugins.plotplus.plot.PlotManager;
import de.craften.plugins.plotplus.plot.world.PlotWorld;
import de.craften.plugins.skyblockplus.SkyblockPlus;
import de.craften.plugins.skyblockplus.skyblock.SkyblockPlayer;
import de.craften.plugins.skyblockplus.skyblock.SkyblockWorld;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * An importer for uSkyblock players and islands.
 */
public class USkyblockImporter {
    private final File islandFolder;
    private final File playerFolder;
    private final SkyblockPlus plugin;
    private final SkyblockWorld world;
    private Map<String, SimplePlayer> players;

    public USkyblockImporter(SkyblockPlus plugin, File uSkyblockFolder, SkyblockWorld world) throws IOException {
        islandFolder = new File(uSkyblockFolder, "islands");
        playerFolder = new File(uSkyblockFolder, "players");
        this.plugin = plugin;
        this.world = world;
        players = new HashMap<>();

        BufferedReader br = new BufferedReader(new FileReader(new File(plugin.getDataFolder(), "players_converted.txt")));
        String line;
        while ((line = br.readLine()) != null) {
            String[] parts = line.replaceAll("\n|\r", "").split("\t");
            players.put(parts[0].toLowerCase(), new SimplePlayer(UUID.fromString(parts[1].replaceFirst("([0-9a-fA-F]{8})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]+)", "$1-$2-$3-$4-$5")), parts[0]));
        }
        br.close();
        plugin.getLogger().info(players.size() + " players loaded");
    }

    public void importPlayers() {
        File[] files = playerFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.endsWith(".yml");
            }
        });

        int count = 1;
        for (File playerFile : files) {
            String login = playerFile.getName().substring(0, playerFile.getName().length() - 4);
            plugin.getLogger().info("Importing player " + count + " of " + files.length + ": " + login);
            ConfigurationSection old = YamlConfiguration.loadConfiguration(playerFile);

            SimplePlayer p = players.get(login.toLowerCase());
            if (p != null) {
                SkyblockPlayer player = plugin.getPlayer(p.getUniqueId());
                ConfigurationSection c = old.getConfigurationSection("player.challenges");
                for (String challenge : c.getKeys(false)) {
                    for (int i = 0; i < c.getInt(challenge + ".timesCompleted"); i++) {
                        player.onChallengeFinished(world.getChallenge(challenge));
                    }
                }
                count++;
                plugin.getLogger().info(count + " of " + files.length + " players imported");
            } else {
                plugin.getLogger().warning("Could not import player " + login + " (player not found)");
            }
        }
    }

    public void importIslands() {
        File[] files = islandFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.endsWith(".yml");
            }
        });
        int count = 1;
        PlotWorld plotWorld = PlotPlus.getApi().getWorld(world.getName());
        PlotManager mgr = plotWorld.getManager();

        for (File islandFile : files) {
            String[] idp = islandFile.getName().substring(0, islandFile.getName().length() - 4).split(",");
            PlotId id;
            try {
                id = new PlotId(Integer.parseInt(idp[0]) / 110, Integer.parseInt(idp[1]) / 110, world.getName());
            } catch (NumberFormatException e) {
                plugin.getLogger().warning("Skipping island file " + islandFile.getName() + " (invalid format)");
                continue;
            }

            ConfigurationSection old = YamlConfiguration.loadConfiguration(islandFile);
            String login = old.getString("party.leader");
            if (login == null) {
                plugin.getLogger().warning("Skipping island file " + islandFile.getName() + " (leader not found)");
                continue;
            }
            SimplePlayer player = players.get(login.toLowerCase());
            if (player != null) {
                try {
                    Plot plot = mgr.createPlot(id.getIdX(), id.getIdZ(), player, plotWorld);
                    for (String member : old.getConfigurationSection("party.members").getKeys(false)) {
                        if (!member.equals(login)) {
                            SimplePlayer helperPlayer = players.get(member);
                            if (helperPlayer != null) {
                                plot.addHelper(helperPlayer);
                            } else {
                                plugin.getLogger().warning("Could not add " + member + " as helper (player not found)!");
                            }
                        }
                    }
                    count++;
                    plugin.getLogger().info(count + " of " + files.length + " islands imported");
                } catch (PersistenceException e) {
                    plugin.getLogger().warning("Can't import island (PersistenceException)");
                }
            } else {
                plugin.getLogger().warning("Can't import island of " + login + " (player not found)");
            }
        }
    }
}
