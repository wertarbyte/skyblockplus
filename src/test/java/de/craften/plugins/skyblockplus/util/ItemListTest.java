package de.craften.plugins.skyblockplus.util;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ItemListTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testParsing() throws Exception {
        assertTrue("Empty String should be parsed as empty ItemList", new ItemList("").isEmpty());

        HashMap<Item, Integer> map = new HashMap<>();
        map.put(new Item(1, 0), 42);
        map.put(new Item(43, 4), 2);
        Set<Map.Entry<Item, Integer>> expected = map.entrySet();
        assertEquals("'1:0:42 43:4:2' should get parsed correctly", expected, new ItemList("1:0:42 43:4:2").getItems());
    }

    @Test
    public void testParsingInvalidSyntax() throws Exception {
        exception.expect(IllegalArgumentException.class);
        new ItemList("42:2!.33:hello");
    }
}