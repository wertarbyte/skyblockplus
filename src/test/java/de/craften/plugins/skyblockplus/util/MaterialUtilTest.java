package de.craften.plugins.skyblockplus.util;

import org.bukkit.Material;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class MaterialUtilTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testNameParsing() {
        assertEquals("Material name should parse correctly", Material.SKULL, MaterialUtil.parse("SKULL"));
        assertEquals("Material name should be case insensitive", Material.DIAMOND_BLOCK, MaterialUtil.parse("diamond_block"));
    }

    @Test
    public void testIdParsing() {
        assertEquals("Material ID should parse correctly", Material.LAVA, MaterialUtil.parse("10"));
        assertEquals("Material ID should parse correctly", Material.SANDSTONE, MaterialUtil.parse("24"));
    }

    @Test
    public void testParsingInvalidName() {
        exception.expect(IllegalArgumentException.class);
        MaterialUtil.parse("strangeBlock");
    }

    @Test
    public void testParsingInvalidId() {
        exception.expect(IllegalArgumentException.class);
        MaterialUtil.parse("72365");
    }
}